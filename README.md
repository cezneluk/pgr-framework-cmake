## Konfigurace

Je potřeba mít nainstalovaný [CMake](https://cmake.org/) a mít funkční překladač pro C/C++.

### Využití knihoven z systému
```
mkdir build && cd build
cmake -DLIBPGR_USE_SYSTEM_LIBS=ON ..
```

### Sestavení závislostí ze zdrojových souborů
```
mkdir build && cd build
cmake -DLIBPGR_USE_SYSTEM_LIBS=OFF -DBUILD_SHARED_LIBS=OFF ..
```

### PGR_FRAMEWORK_ROOT
Oficiální pgr framework využívá proměnnou prostředí `PGR_FRAMEWORK_ROOT` pro určení složky obsahující dodatečné soubory. Navíc je přidána možnost definovat výchozí hodnotu pomocí cmake proměnné `LIBPGR_FRAMEWORK_ROOT_DEFAULT` (ve výchozím stavu se jedná o cestu použitou pro instalaci dodatečných souborů), pokud se tato proměnná nenajde.

### Demo příklady
Automaticky se společně s knihovnou jsou sestaveny i dostupné příklady. Tuto možnost lze explicitně nastavit pomocí parametru `LIBPGR_BUILD_EXAMPLES` (např. `-DLIBPGR_BUILD_EXAMPLES=OFF`). Pro správné fungování je potřeba mít správně zadefinovanou proměnnou prostředí `PGR_FRAMEWORK_ROOT` (viz výše).

## Kompilace

Pro sestavení využijte vybraný sestavovací nástroj (make, ninja apod., viz https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html).

## Instalace

Framework lze nainstalovat do svého operačního systému pomocí pravidla `install` vašeho sestavovacího nástroje.
