set(LIBPGR_DEBUG_TEST_SOURCES "debug-test.cpp")
set(LIBPGR_DEBUG_TEST_SHADERS "color.frag" "colorUniformBlock.vert")

add_executable(pgr-debug-test ${LIBPGR_DEBUG_TEST_SOURCES})
target_link_libraries(pgr-debug-test pgr)
target_include_directories(pgr-debug-test PRIVATE ${LIBPGR_INCLUDE})

foreach(shader ${LIBPGR_DEBUG_TEST_SHADERS})
	copy_file(pgr-debug-test ${shader})
endforeach()
