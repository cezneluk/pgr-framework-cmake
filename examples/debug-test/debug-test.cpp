/**
 * \file debug-test.cpp
 * \brief Does some rendering + causes gl errors
 * \author OpenGL Insights authors, Tomas Barak
 */

/*
This demo was built for the book OpenGL Insights

It was redone to use pgr-framework and its debugging api.

The source code is provided as is, with no warranties, with the only purpose
of showing the debug message callbacks in action.

*/

#include <iostream>

#include "pgr.h"

const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 500;
const char* WIN_TITLE = "PGR - Debug-test";

// Data for drawing Axis
float verticesAxis[] = {
   -20.0f, 0.0f, 0.0f, 1.0f,
    20.0f, 0.0f, 0.0f, 1.0f,

    0.0f, -20.0f, 0.0f, 1.0f,
    0.0f,  20.0f, 0.0f, 1.0f,

    0.0f, 0.0f, -20.0f, 1.0f,
    0.0f, 0.0f,  20.0f, 1.0f
};


float colorAxis[] = {
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f
};

// Data for triangle 1
float vertices1[] = {
    -3.0f, 0.0f, -5.0f, 1.0f,
    -1.0f, 0.0f, -5.0f, 1.0f,
    -2.0f, 2.0f, -5.0f, 1.0f
};


float colors1[] = {
    0.0f, 0.0f, 1.0f, 1.0f,
    0.0f, 1.0f, 0.0f, 1.0f,
    1.0f, 0.0f, 0.0f, 1.0f
};

// Data for triangle 2
float vertices2[] = {
    1.0f, 0.0f, -5.0f, 1.0f,
    3.0f, 0.0f, -5.0f, 1.0f,
    2.0f, 2.0f, -5.0f, 1.0f
};


float colors2[] = {
    1.0f, 0.0f, 0.0f, 1.0f,
    1.0f, 0.0f, 0.0f, 1.0f,
    1.0f, 0.0f, 0.0f, 1.0f
};

// Shader Names
const char *vertexFileName = "colorUniformBlock.vert";
const char *fragmentFileName = "color.frag";

// Program and Shader Identifiers
GLuint p,v,f;

// Vertex Attribute Locations
GLuint vertexLoc, colorLoc;


// Uniform buffer
GLuint uniforms;
GLuint uniformsLoc;
// Uniform variable Locations
GLuint projMatrixLoc, viewMatrixLoc;

// Vertex Array Objects Identifiers
GLuint vao[3];

// storage for Matrices
glm::mat4 projMatrix;
glm::mat4 viewMatrix;

// ----------------------------------------------------

void reshapeCb(int w, int h) {
  glViewport(0, 0, w, h);
  projMatrix =  glm::perspective(60.0f, float(w) / float(h) , 1.0f, 100.0f);

  glBindBuffer(GL_UNIFORM_BUFFER, uniforms);
  glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(projMatrix), glm::value_ptr(projMatrix));
  glBindBuffer(GL_UNIFORM_BUFFER,0);
}

void setupBuffers() {

  GLuint buffers[2];

  glGenVertexArrays(3, vao);
  //
  // VAO for first triangle
  //
  glBindVertexArray(vao[0]);
  // Generate two slots for the vertex and normal buffers
  glGenBuffers(2, buffers);
  // bind buffer for vertices and copy data into buffer
  glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices1), vertices1, GL_STATIC_DRAW);
  glEnableVertexAttribArray(vertexLoc);
  glVertexAttribPointer(vertexLoc, 4, GL_FLOAT, 0, 0, 0);

  // bind buffer for normals and copy data into buffer
  glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(colors1), colors1, GL_STATIC_DRAW);
  glEnableVertexAttribArray(colorLoc);
  glVertexAttribPointer(colorLoc, 4, GL_FLOAT, 0, 0, 0);

  //
  // VAO for second triangle
  //
  glBindVertexArray(vao[1]);
  // Generate two slots for the vertex and normal buffers
  glGenBuffers(2, buffers);

  // bind buffer for vertices and copy data into buffer
  glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_STATIC_DRAW);
  glEnableVertexAttribArray(vertexLoc);
  glVertexAttribPointer(vertexLoc, 4, GL_FLOAT, 0, 0, 0);

  // bind buffer for normals and copy data into buffer
  glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(colors2), colors2, GL_STATIC_DRAW);
  glEnableVertexAttribArray(colorLoc);
  glVertexAttribPointer(colorLoc, 4, GL_FLOAT, 0, 0, 0);

  //
  // This VAO is for the Axis
  //
  glBindVertexArray(vao[2]);
  // Generate two slots for the vertex and normal buffers
  glGenBuffers(2, buffers);
  // bind buffer for vertices and copy data into buffer
  glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(verticesAxis), verticesAxis, GL_STATIC_DRAW);
  glEnableVertexAttribArray(vertexLoc);
  glVertexAttribPointer(vertexLoc, 4, GL_FLOAT, 0, 0, 0);

  // bind buffer for normals and copy data into buffer
  glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(colorAxis), colorAxis, GL_STATIC_DRAW);
  glEnableVertexAttribArray(colorLoc);
  glVertexAttribPointer(colorLoc, 4, GL_FLOAT, 0, 0, 0);

  glBindVertexArray(0);

  //
  // Uniform Block
  //
  glGenBuffers(1,&uniforms);
  glBindBuffer(GL_UNIFORM_BUFFER, uniforms);
  glBufferData(GL_UNIFORM_BUFFER, sizeof(projMatrix) + sizeof(viewMatrix), 0,GL_DYNAMIC_DRAW);

  // upload view matrix
  glBufferSubData(GL_UNIFORM_BUFFER, sizeof(projMatrix), sizeof(viewMatrix), glm::value_ptr(viewMatrix));
  glBindBuffer(GL_UNIFORM_BUFFER,0);

  //glBindBufferRange(GL_UNIFORM_BUFFER, 2, uniforms, 0, sizeof(projMatrix) *2);  //setUniforms();
  glBindBufferBase(GL_UNIFORM_BUFFER, 2, uniforms);
}


void setUniforms() {
  // must be called after glUseProgram
  glUniformMatrix4fv(projMatrixLoc,  1, false, glm::value_ptr(projMatrix));
  glUniformMatrix4fv(viewMatrixLoc,  1, false, glm::value_ptr(viewMatrix));
}

void idleFunc(void) {
  glutPostRedisplay();
}

void renderScene(void) {

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(p);

  glBindVertexArray(vao[0]);
  glDrawArrays(GL_TRIANGLES, 0, 3);

  glBindVertexArray(vao[1]);
  glDrawArrays(GL_TRIANGLES, 0, 3);

  glBindVertexArray(vao[2]);
  glDrawArrays(GL_LINES, 0, 6);

  glBindVertexArray(0);

  glUseProgram(0);

  glutSwapBuffers();
}


void processNormalKeys(unsigned char key, int x, int y) {

  if (key == 27) {
    glDeleteVertexArrays(3,vao);
    glDeleteBuffers(1, &uniforms);
    glDeleteProgram(p);
    glDeleteShader(v);
    glDeleteShader(f);
    exit(0);
  }

  // generate some errors on different keys
  if(key == 'x') {
    glUseProgram(123); // use invalid shader program
    glUniform1f(0, 1.0f); // set uniform, but no valid program is active
  }
  if(key == 'c') {
    glBindVertexArray(123); // bind invalid VAO
    glDrawArrays(GL_POINTS, 0, 2); // draw with invalid vao used
  }
  if(key == 'v') {
    glUseProgram(p); // OK
    glUniform1f(123, 1.0f); // invalid location
    glUseProgram(0); // OK
  }
}

void init() {
  viewMatrix = glm::lookAt(glm::vec3(10.0f, 2.0f, 10.0f), glm::vec3(0.0f, 2.0f, -5.0f), glm::vec3(0.0f, 1.0f, 0.0f));

  GLuint shaders[] = {
    pgr::createShaderFromFile(GL_VERTEX_SHADER, vertexFileName),
    pgr::createShaderFromFile(GL_FRAGMENT_SHADER, fragmentFileName),
    0
  };
  p = pgr::createProgram(shaders);

  vertexLoc = glGetAttribLocation(p, "position");
  colorLoc = glGetAttribLocation(p, "color");

  uniformsLoc = glGetUniformBlockIndex(p,"Matrices");
  glUniformBlockBinding(p, uniformsLoc, 2);

  setupBuffers();
}

int main(int argc, char **argv) {

  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  //glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  //glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
  //glutInitContextProfile (GLUT_CORE_PROFILE);
  glutInitContextFlags(GLUT_DEBUG | GLUT_FORWARD_COMPATIBLE);
  //glutInitContextFlags(GLUT_DEBUG);

  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow(WIN_TITLE);

  glutDisplayFunc(renderScene);
  glutIdleFunc(idleFunc);
  glutReshapeFunc(reshapeCb);
  glutKeyboardFunc(processNormalKeys);

  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  glEnable(GL_DEPTH_TEST);
  glClearColor(1.0,1.0,1.0,1.0);

  // this actually will not be echoed in the default debug level (medium)
  glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_OTHER, 1234, GL_DEBUG_SEVERITY_LOW, -1, "This is just a test");

  init();

  std::cout << "press x, c or v to generate different GL errors and watch the output ..." << std::endl;
  glutMainLoop();

  return 0;
}

