#version 140
in vec3 color_v;
out vec3 color_f;
void main() {
  color_f = color_v;
}
