#version 140
uniform mat4 matrix;
in vec3 vertex;
in vec3 color;
out vec3 color_v;
void main() {
  color_v = color;
  gl_Position = matrix * vec4(vertex, 1.0f);
}
