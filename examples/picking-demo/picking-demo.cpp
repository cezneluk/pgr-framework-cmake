//----------------------------------------------------------------------------------------
/**
 * \file    picking-demo.cpp
 * \author  Tomas Barak, Petr Felkel
 * \date    2011-2012
 * \brief   Per pixel correct picking, done in fragment shader.
 */
//----------------------------------------------------------------------------------------

#include <iostream>

#include "pgr.h"

// projection matrix used by shaders
glm::mat4 projection;
int height;

// our far plane
#define FAR_VAL 20.0f
#define FAR_VAL_S "20.0f"

// our scene is made of several boxes, which will start to rotate on clik
struct Box {
  // costructor with Model matrix as parameter
  Box(const glm::mat4 & model_mat): model(model_mat), rotation(glm::vec3(0, 0, 0)) {}

  // each box has its own transformation matrix
  glm::mat4 model;
  // each box has its rotation vector
  glm::vec3 rotation;

  // all boxes share these variables
  static GLuint draw_shader;        // this shader is used to draw normal scene image (shaded boxes)
  static GLuint pick_shader;        // this shader just "draw" box id, used for picking
  static GLuint buffer_id;          // vertex buffer object (storing vertex coords and normals as a block of bytes)

  // we use 2 shaders, locations can differ
  static GLint model_mat_loc_draw;  // location of model matrix in draw shader
  static GLint proj_mat_loc_draw;   // location of projection matrix in draw shader
  static GLint pos_loc_draw;        // location of position attribute in draw shader
  static GLint nor_loc_draw;        // location of normal attribute in draw shader
  static GLuint vertexArray_draw;   // Vertex Array Object (buffer binding to attributes + locations)

  static GLint model_mat_loc_pick;  // location of model matrix in pick shader
  static GLint proj_mat_loc_pick;   // location of projection matrix in pick shader
  static GLint id_loc_pick;         // location of ID uniform in pick shader
  static GLint pos_loc_pick;        // location of position attribute in pick shader
  static GLuint vertexArray_pick;   // Vertex Array Object (buffer binding to attributes + locations)
};

// Box class variables (static)
GLuint Box::draw_shader   = 0;
GLuint Box::pick_shader   = 0;
GLuint Box::buffer_id     = 0;

GLint Box::model_mat_loc_draw = 0;
GLint Box::proj_mat_loc_draw  = 0;
GLint Box::pos_loc_draw   = 0;
GLint Box::nor_loc_draw   = 0;

GLint Box::model_mat_loc_pick = 0;
GLint Box::proj_mat_loc_pick  = 0;
GLint Box::id_loc_pick    = 0;
GLint Box::pos_loc_pick   = 0;

GLuint Box::vertexArray_draw = 0; // Vertex Array Object (buffer binding to attributes + locations)
GLuint Box::vertexArray_pick = 0; // Vertex Array Object (buffer binding to attributes + locations)

// few randomly spawn boxes
Box boxes[] = {
  Box(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, -10))),
  Box(glm::translate(glm::mat4(1.0f), glm::vec3(-2.5, 1.5, -8))),
  Box(glm::translate(glm::mat4(1.0f), glm::vec3(-2.5, -1, -8))),
  Box(glm::translate(glm::mat4(1.0f), glm::vec3(2.5, -1.5, -8))),
  Box(glm::translate(glm::mat4(1.0f), glm::vec3(2.5, -1, -10)))
};

extern float box_data[]; // raw box vertices and normals are listed in separate file (box_data.cpp)

// vertex shader used for drawing - calculates position and normal
static const std::string drawVtxShader(
    "#version 140\n"
    "uniform mat4 M;\n"
    "uniform mat4 P;\n"
    "in vec3 position;\n"
    "in vec3 normal;\n"
    "smooth out vec3 theNormal;\n"
    "smooth out vec3 thePosition;\n"
    "void main()\n"
    "{\n"
    "  vec4 pos4 = M * vec4(position, 1);\n"
    "  gl_Position = P * pos4;\n"
    //"  thePosition = pos4.xyz / pos4.w;\n"  // It is hard to get w <> 1 after multiplication by M - e.g. shear or asymetric scale
    "  thePosition = pos4.xyz;\n"
    "  theNormal = (M * vec4(normal, 0)).xyz;\n"
    "}\n"
);

// fragment shader uses normal, position, and hardcoded light to calculate simple shading
static const std::string drawFragmentShader(
    "#version 140\n"
    "smooth in vec3 theNormal;\n"
    "smooth in vec3 thePosition;\n"
    "out vec4 outputColor;\n"
    "const vec3 lightPos = vec3(0, 2, 0);\n"
    "void main()\n"
    "{\n"
    "  outputColor = vec4(1, 1, 1, 1) * max(0.0, dot(normalize(theNormal), normalize(lightPos - thePosition)));\n"
    "}\n"
);

// pick vtx shader does not have to calculate normal, we calculate position and pass depth to frag shader
static const std::string pickVtxShader(
    "#version 140\n"
    "uniform mat4 M;\n"
    "uniform mat4 P;\n"
    "in vec3 position;\n"
    "smooth out float depth;\n"
    "void main()\n"
    "{\n"
    "  vec4 pos4 = M * vec4(position, 1);\n"
    "  gl_Position = P * pos4;\n"
    "  depth = pos4.z;\n"
//"  depth = pos4.z / pos4.w;\n"  // It is hard to get w <> 1 after multiplication by M - e.g. shear or asymetric scale
    "}\n"
);

/* our picking fragmet shader writes 3 importatnt values on output
 * The id read from uniform variable is stored in the red channel.
 * The green channel stores 1 (which indicates something has been drawn).
 * The blue channel contains dept normalized to the maximum depth
 *   - this is not mandatory for picking, but it is nice to show that more information can be encoded to our output
 * The alpha channel is unused. We can use it for example if we have more than 256 object ids.
 * Remember: one channel is just 8bit number in default!
 * Remember#2: Shaders write floating values in [0,1] interval, but we read [0,255] on CPU.
 */
static const std::string pickFragmentShader(
    "#version 140\n"
    "uniform float id;\n"
    "smooth in float depth;\n"
    "out vec4 outId;\n"
    "void main()\n"
    "{\n"
    "  outId = vec4(id, 1, -depth / "FAR_VAL_S", 0);\n"
    "}\n"
);

void init() {
  // build draw shader and query all attribute and uniform locations
  std::vector<GLuint> shaderList;
  shaderList.push_back(pgr::createShaderFromSource(GL_VERTEX_SHADER, drawVtxShader));
  shaderList.push_back(pgr::createShaderFromSource(GL_FRAGMENT_SHADER, drawFragmentShader));

  Box::draw_shader = pgr::createProgram(shaderList);
  Box::model_mat_loc_draw = glGetUniformLocation(Box::draw_shader, "M");
  Box::proj_mat_loc_draw = glGetUniformLocation(Box::draw_shader, "P");
  Box::pos_loc_draw = glGetAttribLocation(Box::draw_shader, "position");
  Box::nor_loc_draw = glGetAttribLocation(Box::draw_shader, "normal");

  // build pick shader and query all attribute and uniform locations
  shaderList.clear();
  shaderList.push_back(pgr::createShaderFromSource(GL_VERTEX_SHADER, pickVtxShader));
  shaderList.push_back(pgr::createShaderFromSource(GL_FRAGMENT_SHADER, pickFragmentShader));

  Box::pick_shader = pgr::createProgram(shaderList);
  Box::model_mat_loc_pick = glGetUniformLocation(Box::pick_shader, "M");
  Box::proj_mat_loc_pick = glGetUniformLocation(Box::pick_shader, "P");
  Box::id_loc_pick = glGetUniformLocation(Box::pick_shader, "id");
  Box::pos_loc_pick = glGetAttribLocation(Box::pick_shader, "position");

  // create VAO for drawing the boxes
  glGenVertexArrays(1, &Box::vertexArray_draw);
  glBindVertexArray(Box::vertexArray_draw);
  // build vertex buffer and upload data to it
  glGenBuffers(1, &Box::buffer_id);
  glBindBuffer(GL_ARRAY_BUFFER, Box::buffer_id);
  glBufferData(GL_ARRAY_BUFFER, 6*6*6 * sizeof(float), box_data, GL_STATIC_DRAW);
  // enable position and normal arrays
  glEnableVertexAttribArray(Box::pos_loc_draw);
  glEnableVertexAttribArray(Box::nor_loc_draw);

  // describe the attribute position and normal in the vertex buffer and its layout
  glVertexAttribPointer(Box::pos_loc_draw, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glVertexAttribPointer(Box::nor_loc_draw, 3, GL_FLOAT, GL_FALSE, 0, (void*)(3 * 6 * 6 * sizeof(float)));
  glBindVertexArray(0);

  // create VAO for picking
  glGenVertexArrays(1, &Box::vertexArray_pick);
  glBindVertexArray(Box::vertexArray_pick);
  // we use the same vertex data for picking
  glBindBuffer(GL_ARRAY_BUFFER, Box::buffer_id);
  // we use only position attribute
  glEnableVertexAttribArray(Box::pos_loc_pick);

  // describe the attribute position in the vertex buffer and its layout
  glVertexAttribPointer(Box::pos_loc_pick, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glBindVertexArray(0);

  glDisable(GL_CULL_FACE); // draw both back and front faces
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
}

void do_picking(int button, int x, int y) {
  // it is not necessary to draw the whole window, we could shrink our viewport, if we want to optimize our code
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(Box::pick_shader);

  glUniformMatrix4fv(Box::proj_mat_loc_pick, 1, GL_FALSE, glm::value_ptr(projection));

  glBindVertexArray(Box::vertexArray_pick);

  for(int b = 0; b < sizeof(boxes) / sizeof(Box); ++b) {
    // to every object we pass its id as an uniform variable
    // is is simpler to encode our id as float value (more portable shader code)
    // other than float is possible for non-float targets, such as textures...
    glUniform1f(Box::id_loc_pick, (float)b / 255);
    glUniformMatrix4fv(Box::model_mat_loc_pick, 1, GL_FALSE, glm::value_ptr(boxes[b].model));
    glDrawArrays(GL_TRIANGLES, 0, 6 * 6); // six vertices per box side, six sides of the box
  }

  glBindVertexArray(0);

  // read one pixel under the mouse cursor
  unsigned char pixel[4];
  glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixel);  // does not use shaders - allows read of four RGBA BYTEs

  // the buffer was cleared to zeros
  if(pixel[1] == 0) {
    std::cout << "clicked on background" << std::endl;
  } else {
    std::cout << "clicked on object " << (int)pixel[0] << " in depth " << (float)pixel[2] * FAR_VAL / 255 << std::endl;
    if(button == GLUT_LEFT_BUTTON)
      boxes[pixel[0]].rotation = glm::vec3(1, 1, 1); // start box rotation on left mouse click
    else
      boxes[pixel[0]].rotation = glm::vec3(0, 0, 0); // and stop it on the right click
  }
}

void timer_callback(int) {
  // animate boxes
  for(int b = 0; b < sizeof(boxes) / sizeof(Box); ++b) {
    if(boxes[b].rotation.x == 0 && boxes[b].rotation.y == 0 && boxes[b].rotation.z == 0)
      continue;

    boxes[b].model = glm::rotate(boxes[b].model, 1.0f, boxes[b].rotation);
  }

  glutTimerFunc(33, timer_callback, 0);
  glutPostRedisplay();
}

void keyboard_callback(unsigned char key, int x, int y) {
  // exit on escape
  if(key == 27)
    exit(0);
}

void mouse_callback(int button, int state, int x, int y) {
  // do picking only on mouse down
  if(state == GLUT_DOWN) {
    do_picking(button, x, height - 1 - y); // recalculate y, as glut has origin in upper left corner
    glutPostRedisplay();
  }
}

void display_callback() {
  // we render our scene as usual
  glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(Box::draw_shader);

  glUniformMatrix4fv(Box::proj_mat_loc_draw, 1, GL_FALSE, glm::value_ptr(projection));

  glBindVertexArray( Box::vertexArray_draw );
  for(int b = 0; b < sizeof(boxes) / sizeof(Box); ++b) {
    glUniformMatrix4fv(Box::model_mat_loc_draw, 1, GL_FALSE, glm::value_ptr(boxes[b].model));
    glDrawArrays(GL_TRIANGLES, 0, 6 * 6); // six vertices per box side, six sides of the box
  }
  glBindVertexArray(0);

  glutSwapBuffers();
}

void reshape_callback(int w, int h) {
  glViewport(0, 0, (GLsizei) w, (GLsizei) h);
  float aspect_ratio = (float)w/(float)h;
  projection = glm::perspective(45.0f, aspect_ratio, 0.1f, 100.0f);
  height = h;
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE  | GLUT_DEPTH );
  glutInitWindowSize(800, 600);
  glutCreateWindow("Picking demo");

  glutDisplayFunc(display_callback);
  glutReshapeFunc(reshape_callback);
  glutKeyboardFunc(keyboard_callback);
  glutMouseFunc(mouse_callback);
  glutTimerFunc(33, timer_callback, 0);

  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  init();

  std::cout << "USAGE: use the left and right mouse button to start/stop rotation of a cube" << std::endl;
  glutMainLoop();
  return 0;
}
