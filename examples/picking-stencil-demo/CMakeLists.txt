set(LIBPGR_PICKING_STENCIL_DEMO_SOURCES "picking-stencil-demo.cpp" "box_data.cpp")

add_executable(pgr-picking-stencil-demo ${LIBPGR_PICKING_STENCIL_DEMO_SOURCES})
target_link_libraries(pgr-picking-stencil-demo pgr)
target_include_directories(pgr-picking-stencil-demo PRIVATE ${LIBPGR_INCLUDE})
