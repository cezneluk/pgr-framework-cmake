/**
 * \file primitives-bowl.cpp
 * \brief Displays a bowl using triangle strips. Procedural mesh generation demo.
 * \author Jaroslav Sloup, Tomas Barak
 */

#include <cmath>
#include <iostream>
#include <string>

#include "trackball.h"
#include "pgr.h"

#if _MSC_VER
#define snprintf _snprintf
#endif

const int WIN_WIDTH = 500;
const int WIN_HEIGHT = 500;
const char* WIN_TITLE = "PGR - Primitives - bowl";
const unsigned int REFRESH_INTERVAL = 33;
const int NLIGHTS = 3;
double elapsedTime = 0.0;
glm::mat4 projection = glm::mat4(1.0f);
glm::mat4 model = glm::mat4(1.0f);

// virtual trackball related stuff
pgr::CQuaternionTrackball trackball;	// trackball class -> uses quaterninons to rotate the scene
int startGrabX, startGrabY;				// trackball starting point
int endGrabX, endGrabY;					// trackball end point
int winWidth, winHeight;				// window width & height
glm::mat4 trackballRotation;			// trackball rotation matrix
glm::mat4 modelRotation;				// model rotation matrix

GLuint bowlShaderProgram = 0;
GLuint bowlVbo = 0;
GLuint bowlTrianglesEao = 0;
GLuint bowlTrianglesVao = 0;
GLenum displayMode = GL_LINE;

// number of points in profile
#define PROFILE_POINTS_COUNT 13
// profile is defined by height and related radius
float height[PROFILE_POINTS_COUNT] = {0.1f, 0.0f, 0.2f, 0.5f, 0.6f, 0.7f, 0.9f, 1.4f, 1.5f, 1.6f, 1.5f, 1.4f, 0.9f};
float radius[PROFILE_POINTS_COUNT] = {0.0f, 0.7f, 0.1f, 0.1f, 0.2f, 0.1f, 0.1f, 0.8f, 0.85f, 1.0f, 0.8f, 0.75f, 0.0f};
// number of rotation steps
#define ROT_STEPS_COUNT 25

// array of bowl vertices
const unsigned int nBowlVertices = 2*(PROFILE_POINTS_COUNT-1) * ROT_STEPS_COUNT;
const unsigned nBowlAttribsPerVertex = 6; // x,y,z position, nx,ny,nz normal vector
float bowlVertices[nBowlVertices * nBowlAttribsPerVertex];

// indices used to draw cube as GL_TRIANGLE_STRIP
const unsigned nBowlTriangleIdx = 2*(ROT_STEPS_COUNT+1);
unsigned short bowlTriangleIdx[nBowlTriangleIdx*(PROFILE_POINTS_COUNT-1)];

// starting indices of the triangle strips
const GLvoid* indicesStartAt[PROFILE_POINTS_COUNT-1];
// number of vertices in each strip
static GLsizei primsCount[PROFILE_POINTS_COUNT-1];

GLint colorLoc = -1;
glm::vec3 sphereColor(1.0f, 1.0f, 1.0f);

struct Light {
  Light(float r = 1.0f, float g = 1.0f, float b = 1.0f):
    color(r, g, b, 0.0f), position(0.0f, 0.0f, 0.0f, 1.0f) {}

  glm::vec4 position;
  glm::vec4 color;
};

Light lights[NLIGHTS] = {
  Light(1.0f, 0.25f, 0.25f),
  Light(0.25f, 1.0f, 0.25f),
  Light(1.0f, 1.0f, 1.0f)
};

// vertex shader just sends position and normal to the fragment shader
std::string vertexShaderSrc =
    "#version 140\n"
    "uniform mat4 modelMat;\n"
    "uniform mat4 projMat;\n"
    "in vec3 vertex;\n"
    "in vec3 normal;\n"
    "smooth out vec3 normal_v;\n"
    "out vec3 position_v;\n"
    "void main()\n"
    "{\n"
    "  vec4 nor4 = modelMat * vec4(normal, 0.0f);\n"
    "  normal_v = nor4.xyz;\n"
    "  vec4 pos4 = modelMat * vec4(vertex, 1.0f);\n"
    "  position_v = pos4.xyz / pos4.w;\n"
    "  gl_Position = projMat * pos4;\n"
    "}\n"
    ;

// takes interpolated normal and position and calculates per pixel diffuse lighting
std::string fragmentShaderSrc =
    "#version 140\n"
    "#define NLIGHTS 3\n"
    "struct Light {\n"
    "  vec3 position;\n"
    "  vec3 color;\n"
    "};\n"
    "uniform Light lights[NLIGHTS];\n"
    "smooth in vec3 normal_v;\n"
    "in vec3 position_v;\n"
    "uniform vec3 color;\n"
    "out vec3 color_f;\n"
    "void main()\n"
    "{\n"
    "  vec3 diffuse = color;\n"
    "  vec3 color = vec3(0.0f);\n"
    "  vec3 N = normalize(normal_v);\n"
    "  for(int l = 0; l < NLIGHTS; ++l) {\n"
    "    vec3 L = lights[l].position - position_v;\n"
    "    float dist = length(L);\n"
    "    float attenuation = min(1.0f / (dist * dist), 1.0f);\n"
    "    color += diffuse * lights[l].color * max(dot(N, L) / dist, 0.0f) * attenuation;\n"
    "  }\n"
    "  color_f = 0.85*color + vec3(0.15f, 0.15f, 0.15f);\n"
    "}\n"
    ;

void generateBowl(void) {

  // precomputed points on circle
  float points[ROT_STEPS_COUNT*2];

#define PI 3.1415162
#define STEP 2.0*PI / ROT_STEPS_COUNT

  // precompute points on the circle
  float angle = 0;
  for(int i=0; i<ROT_STEPS_COUNT; i++, angle+=(float)(STEP)) {
    points[i*2] = sin(angle);
    points[i*2+1] = cos(angle);
  }

  float height1Val, radius1Val, height2Val, radius2Val, nx, ny, nz;

  unsigned int vertIdx = 0;
  unsigned int index, idx = 0;

  float dh, dr;

  // loop over all rings
  for(unsigned int h=0; h<PROFILE_POINTS_COUNT-1; h++) {

    if(h != PROFILE_POINTS_COUNT-1) {
      primsCount[h] = nBowlTriangleIdx;
      indicesStartAt[h] = (GLvoid *)(h*nBowlTriangleIdx*sizeof(unsigned short));
    }

    height1Val = height[h];
    radius1Val = radius[h];
    height2Val = height[h+1];
    radius2Val = radius[h+1];
    dr = radius2Val - radius1Val;
    dh = height2Val - height1Val;

    // each ring is rendered as a triangle strip
    // start of the GL_TRIANGLE_STRIP
    for(int i=0; i<ROT_STEPS_COUNT; i++) {

      index = nBowlAttribsPerVertex*vertIdx;

      // add a new vertex coordinates
      bowlVertices[index+0] = radius1Val*points[i*2];	// x
      bowlVertices[index+1] = height1Val-0.75f;			// y
      bowlVertices[index+2] = radius1Val*points[i*2+1];	// z

      nx = dh*points[i*2];
      ny = -dr;
      nz = dh*points[i*2+1];

      // normal vector
      bowlVertices[index+3] = nx;
      bowlVertices[index+4] = ny;
      bowlVertices[index+5] = nz;

      bowlTriangleIdx[idx++] = vertIdx;
      vertIdx++;

      index += nBowlAttribsPerVertex;

      // vertex coordinates
      bowlVertices[index+0] = radius2Val*points[i*2];	// x
      bowlVertices[index+1] = height2Val-0.75f;			// y
      bowlVertices[index+2] = radius2Val*points[i*2+1];	// z

      // normal vector
      bowlVertices[index+3] = nx;
      bowlVertices[index+4] = ny;
      bowlVertices[index+5] = nz;

      bowlTriangleIdx[idx++] = vertIdx;

      vertIdx++;
    }
    bowlTriangleIdx[idx++] = vertIdx - 2*ROT_STEPS_COUNT;
    bowlTriangleIdx[idx++] = vertIdx - 2*ROT_STEPS_COUNT+1;;
    // end of the GL_TRIANGLE_STRIP
  }
}

void init() {

  // opengl setup
  glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
  //glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glPointSize(5.0f);

  // load shader
  std::vector<GLuint> shaders;
  shaders.push_back(pgr::createShaderFromSource(GL_VERTEX_SHADER, vertexShaderSrc));
  shaders.push_back(pgr::createShaderFromSource(GL_FRAGMENT_SHADER, fragmentShaderSrc));
  bowlShaderProgram = pgr::createProgram(shaders);

  // handles to vertex shader inputs
  GLint vertexLoc = glGetAttribLocation(bowlShaderProgram, "vertex");
  GLint normalLoc = glGetAttribLocation(bowlShaderProgram, "normal");
  colorLoc = glGetUniformLocation(bowlShaderProgram, "color");

  generateBowl();

  // buffer for vertices
  glGenBuffers(1, &bowlVbo);
  glBindBuffer(GL_ARRAY_BUFFER, bowlVbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(bowlVertices), bowlVertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // buffer for triangle indices
  glGenBuffers(1, &bowlTrianglesEao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bowlTrianglesEao);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(bowlTriangleIdx), bowlTriangleIdx, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  // indexed triangle strips
  glGenVertexArrays(1, &bowlTrianglesVao);
  glBindVertexArray(bowlTrianglesVao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bowlTrianglesEao);
  glBindBuffer(GL_ARRAY_BUFFER, bowlVbo);
  glEnableVertexAttribArray(vertexLoc);
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, nBowlAttribsPerVertex * sizeof(float), (void *)(0));
  glEnableVertexAttribArray(normalLoc);
  glVertexAttribPointer(normalLoc, 3, GL_FLOAT, GL_FALSE, nBowlAttribsPerVertex * sizeof(float), (void *)(nBowlAttribsPerVertex/2 * sizeof(float)));

  glBindVertexArray(0);
  CHECK_GL_ERROR();

  // initialize trackball -> drag from point [0,0] to point [0,0]
  trackball.setRotation(0.0f, 0.0f, 0.0f, 0.0f);
}

// deletes allocated buffers
void cleanup() {

  glDeleteVertexArrays(1, &bowlTrianglesVao);
  glDeleteBuffers(1, &bowlTrianglesEao);
  glDeleteBuffers(1, &bowlVbo);
  pgr::deleteProgramAndShaders(bowlShaderProgram);
}

void switchMode() {

  switch(displayMode) {
    case GL_POINT:
      displayMode = GL_LINE;
      break;
    case GL_LINE:
      displayMode = GL_FILL;
      break;
    case GL_FILL:
      displayMode = GL_POINT;
      break;
    default:;
  }
}

void refreshCb(int) {

  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);
  elapsedTime = 0.001 * (double)glutGet(GLUT_ELAPSED_TIME);	// milliseconds => seconds

  float rotAngleDeg = (float)(elapsedTime * 60.0f);		// rotate 60 degrees per second
  float l0rotAngleDeg = (float)(elapsedTime * 60.0f);	// rotate 60 degrees per second
  float l1rotAngleDeg = (float)(elapsedTime * 40.0f);	// rotate 40 degrees per second
  float radius = 2.5f;
  float bowlCenterOff = -5.0f;

  lights[0].position = glm::vec4(radius * cos(glm::radians(l0rotAngleDeg)), radius * sin(glm::radians(l0rotAngleDeg)), bowlCenterOff, 1.0f);
  lights[1].position = glm::vec4(radius * cos(glm::radians(l1rotAngleDeg)), 0.0f, radius * sin(glm::radians(l1rotAngleDeg)) + bowlCenterOff, 1.0f);
  lights[2].position = glm::vec4(0.0f, radius * cos(glm::radians(l1rotAngleDeg)), radius * sin(glm::radians(l1rotAngleDeg)) + bowlCenterOff, 1.0f);

  model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, bowlCenterOff));	// move cup 5 units back
  model = glm::rotate(model, 30.0f, glm::vec3(1.0f, 0.0f, 0.0f));			        // tilt cup by 30 degrees
  model = glm::scale(model, glm::vec3(1.75f, 1.75f, 1.75f));					        // scale

  modelRotation = glm::rotate(glm::mat4(1.0f), rotAngleDeg, glm::vec3(0.0f, 1.0f, 0.0f));		    // the animation

  glutPostRedisplay();
}

void displayCb() {

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // compose model transformations
  glm::mat4 newModel = model * trackballRotation * modelRotation;

  glUseProgram(bowlShaderProgram);
  glUniformMatrix4fv(glGetUniformLocation(bowlShaderProgram, "modelMat"), 1, GL_FALSE, glm::value_ptr(newModel));
  glUniformMatrix4fv(glGetUniformLocation(bowlShaderProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(projection));

  for(int light = 0; light < NLIGHTS; ++light) {
    char buf[255];
    snprintf(buf, 255, "lights[%i].position", light);
    glUniform3fv(glGetUniformLocation(bowlShaderProgram, buf), 1, glm::value_ptr(lights[light].position));
    snprintf(buf, 255, "lights[%i].color", light);
    glUniform3fv(glGetUniformLocation(bowlShaderProgram, buf), 1, glm::value_ptr(lights[light].color));
  }

  glPolygonMode(GL_FRONT_AND_BACK, displayMode);

  glBindVertexArray(bowlTrianglesVao);

  glUniform3f(colorLoc, 1.0, 1.0, 0.0);
  // draws multiple triangles trips, primsCount[i] contains number of vertices in each strip,
  // vertex indices start at the indicesStartAt[i] index
  glMultiDrawElements(GL_TRIANGLE_STRIP, primsCount, GL_UNSIGNED_SHORT, indicesStartAt, PROFILE_POINTS_COUNT-1);
  // void glMultiDrawElements(GLenum mode, GLsizei *count, GLenum type, const GLvoid **indices, GLsizei primcount);
  // command has the same effect as the following sequence:
  //   for (i = 0; i < primcount; i++) {
  //     if (count[i] > 0)
  //       glDrawElements(mode, count[i], type, indices[i]);
  //   }

  glutSwapBuffers();
}

void reshapeCb(int w, int h) {

  winWidth = w;
  winHeight = h;

  glViewport(0, 0, w, h);
  projection =  glm::perspective(60.0f, float(w) / float(h) , 1.0f, 10.0f);
}

void keyboardCb(unsigned char key, int x, int y) {

  switch (key) {
    case 27:
      glutLeaveMainLoop();
      break;
    case ' ':
      switchMode();
      break;
  }
}

// mouse motion within the window with any button pressed (mouse drag)
void mouseMotionCb(int x, int y) {

  endGrabX = x;
  endGrabY = y;

  if(startGrabX != endGrabX || startGrabY != endGrabY) {

    /* get rotation from trackball using quaternion */
    trackball.addRotation(startGrabX, startGrabY, endGrabX, endGrabY, winWidth, winHeight);

    /* build rotation matrix from quaternion */
    trackball.getRotationMatrix(trackballRotation);

    startGrabX = endGrabX;
    startGrabY = endGrabY;

    glutPostRedisplay();
  }
}

//mouse button pressed within a window or released
void mouseCb(int button, int state, int x, int y) {

  if(button == GLUT_LEFT_BUTTON) {
    if(state == GLUT_DOWN) {
      startGrabX = x;
      startGrabY = y;
      glutMotionFunc(mouseMotionCb);
      return;
    } else {
      glutMotionFunc(NULL);
    }
  }
}

int main(int argc, char** argv) {

  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow(WIN_TITLE);

  glutDisplayFunc(displayCb);
  glutReshapeFunc(reshapeCb);
  glutKeyboardFunc(keyboardCb);
  glutMouseFunc(mouseCb);
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);

  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  init();

  std::cout << "Use space to cycle through modes, Esc to quit." << std::endl;

  glutMainLoop();
  cleanup();
  return 0;
}
