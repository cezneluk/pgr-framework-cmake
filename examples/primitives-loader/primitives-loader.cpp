/**
 * \file primitives-loader.cpp
 * \brief Loads and displays model from external file.
 * \author Tomas Barak
 */

#include <cmath>
#include <iostream>
#include <string>

#include "pgr.h"

const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 500;
const char * WIN_TITLE = "PGR - Primitives - Loader";
const unsigned int REFRESH_INTERVAL = 20;
glm::mat4 projection = glm::mat4(1.0f);
glm::mat4 model = glm::mat4(1.0f);
glm::mat4 view = glm::mat4(1.0f);
unsigned numTriangles = 0;

GLenum fillModes[] = {
  GL_FILL, GL_LINE, GL_POINT
};
unsigned currentFillMode = 0;

struct Resources {
  GLuint shader;
  GLuint vbo;
  GLuint eao;
  GLuint vao;
} resources;

std::string vertexShaderSrc =
  "#version 140\n"
  "uniform mat4 matrix;\n"
  "in vec3 vertex;\n"
  "in vec3 normal;\n"
  "out vec3 color_v;\n"
  "void main()\n"
  "{\n"
  "  color_v = mix(vec3(max(dot(normal, vec3(0.0, 1.0, 0.0)), 0.0)), vec3(0.2), 0.5);\n" // simulate some lighting, using simple diffuse + ambient
  "  gl_Position = matrix * vec4(vertex, 1.0f);\n"
  "}\n"
  ;

std::string fragmentShaderSrc =
  "#version 140\n"
  "in vec3 color_v;\n"
  "out vec3 color_f;\n"
  "void main()\n"
  "{\n"
  "  color_f = color_v;\n"
  "}\n"
  ;

/** Load mesh using assimp library
 * \param filename [in] file to open/load
 * \param shader [in] vao will connect loaded data to shader
 * \param vbo [out] vertex and normal data |VVVVV...|NNNNN...| (no interleaving)
 * \param eao [out] triangle indices
 * \param vao [out] vao connects data to shader input
 * \param numTriangles [out] how many triangles have been loaded and stored into index array eao
 */
bool loadMesh(const std::string & fileName, GLuint shader, GLuint * vbo, GLuint * eao, GLuint * vao, unsigned * numTriangles) {
  Assimp::Importer importer;
  importer.SetPropertyInteger(AI_CONFIG_PP_PTV_NORMALIZE, 1); // Unitize object in size (scale the model to fit into (-1..1)^3)
  // Load asset from the file - you can play with various processing steps
  const aiScene * scn = importer.ReadFile(fileName.c_str(), 0
      | aiProcess_Triangulate             // Triangulate polygons (if any).
      | aiProcess_PreTransformVertices    // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
      | aiProcess_GenSmoothNormals        // Calculate normals per vertex.
      | aiProcess_JoinIdenticalVertices);
  // abort if the loader fails
  if(!scn) {
    std::cerr << "assimp error: " << importer.GetErrorString() << std::endl;
    return false;
  }
  // some formats store whole scene (multiple meshes and materials, lights, cameras, ...) in one file, we cannot handle that in our simplified example
  if(scn->mNumMeshes != 1) {
    std::cerr << "this simplified loader can only process files with only one mesh" << std::endl;
    return false;
  }
  // in this phase we know we have one mesh in our loaded scene, we can directly copy its data to opengl ...
  const aiMesh * mesh = scn->mMeshes[0];

  // vertex buffer object, store all vertex positions and normals
  glGenBuffers(1, vbo);
  glBindBuffer(GL_ARRAY_BUFFER, *vbo);
  glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float) * mesh->mNumVertices * 2, 0, GL_STATIC_DRAW); // allocate memory for vertices and normals
  // first store all vertices
  glBufferSubData(GL_ARRAY_BUFFER, 0, 3 * sizeof(float) * mesh->mNumVertices, mesh->mVertices);
  // then all normals
  glBufferSubData(GL_ARRAY_BUFFER, 3 * sizeof(float) * mesh->mNumVertices, 3 * sizeof(float) * mesh->mNumVertices, mesh->mNormals);

  // copy all mesh faces into one big array (assimp supports faces with ordinary number of vertices, we use only 3 -> triangles)
  unsigned * indices = new unsigned[mesh->mNumFaces * 3];
  for(unsigned f = 0; f < mesh->mNumFaces; ++f) {
    indices[f * 3 + 0] = mesh->mFaces[f].mIndices[0];
    indices[f * 3 + 1] = mesh->mFaces[f].mIndices[1];
    indices[f * 3 + 2] = mesh->mFaces[f].mIndices[2];
  }
  // copy our temporary index array to opengl and free the array
  glGenBuffers(1, eao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *eao);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned) * mesh->mNumFaces, indices, GL_STATIC_DRAW);
  delete [] indices;

  // handles to vertex shader inputs
  GLint vertexLoc = glGetAttribLocation(shader, "vertex");
  GLint normalLoc = glGetAttribLocation(shader, "normal");

  glGenVertexArrays(1, vao);
  glBindVertexArray(*vao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *eao); // bind our element array buffer (indices) to vao
  glBindBuffer(GL_ARRAY_BUFFER, *vbo);
  glEnableVertexAttribArray(vertexLoc);
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(normalLoc);
  glVertexAttribPointer(normalLoc, 3, GL_FLOAT, GL_FALSE, 0, (void*)(3 * sizeof(float) * mesh->mNumVertices));
  glBindVertexArray(0);

  *numTriangles = mesh->mNumFaces;
  return true;
}

bool init() {
  // opengl setup
  glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glPointSize(5.0f);

  // load shader
  GLuint shaders[] = {
    pgr::createShaderFromSource(GL_VERTEX_SHADER, vertexShaderSrc),
    pgr::createShaderFromSource(GL_FRAGMENT_SHADER, fragmentShaderSrc),
    0
  };
  resources.shader = pgr::createProgram(shaders);

  //std::string meshFile = pgr::frameworkRoot() + "data/cow-nonormals.obj";
  //std::string meshFile = pgr::frameworkRoot() + "data/teddy.obj";
  //std::string meshFile = "fels.3ds";
  std::string meshFile = "duck.dae";
  if(!loadMesh(meshFile, resources.shader, &resources.vbo, &resources.eao, &resources.vao, &numTriangles)) {
    return false;
  }

  return true;
}

// deletes allocated buffers, objects
void cleanup() {
  glDeleteVertexArrays(1, &resources.vao);
  glDeleteBuffers(1, &resources.eao);
  glDeleteBuffers(1, &resources.vbo);
  pgr::deleteProgramAndShaders(resources.shader);
}

void switchMode() {
  currentFillMode = (currentFillMode + 1) % (sizeof(fillModes) / sizeof(*fillModes));
  glPolygonMode(GL_FRONT_AND_BACK, fillModes[currentFillMode]);
}

void refreshCb(int) {
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);
  double elapsedTime = 0.001 * (double)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

  float rotAngleDeg = elapsedTime * 60.0f; // rotate 60 degrees per second
  model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -2.5f)); // move units back
  model = glm::rotate(model, 30.0f, glm::vec3(1.0f, 0.0f, 0.0f)); // tilt
  model = glm::rotate(model, rotAngleDeg, glm::vec3(0.0f, 1.0f, 0.0f)); // the animation

  glutPostRedisplay();
}

void displayCb() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glm::mat4 matrix = projection * view * model;
  glUseProgram(resources.shader);
  GLint matrixLoc = glGetUniformLocation(resources.shader, "matrix");
  glUniformMatrix4fv(matrixLoc, 1, GL_FALSE, glm::value_ptr(matrix));

  glBindVertexArray(resources.vao);
  glDrawElements(GL_TRIANGLES, numTriangles * 3, GL_UNSIGNED_INT, 0);

  glutSwapBuffers();
}

void reshapeCb(int w, int h) {
  glViewport(0, 0, w, h);
  projection =  glm::perspective(60.0f, float(w) / float(h) , 1.0f, 10.0f);
}

void keyboardCb(unsigned char key, int x, int y) {
  switch (key) {
    case 27:
      glutLeaveMainLoop();
      break;
    case ' ':
      switchMode();
      break;
  }
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow(WIN_TITLE);

  glutDisplayFunc(displayCb);
  glutReshapeFunc(reshapeCb);
  glutKeyboardFunc(keyboardCb);
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);

  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  if(!init()) {
    pgr::dieWithError("init failed, cannot continue");
  }

  std::cout << "Use space to cycle through modes, Esc to quit." << std::endl;

  glutMainLoop();
  cleanup();
  return 0;
}
