/**
 * \file primitives-sphere.cpp
 * \brief Displays a sphere using triangles (recurcive subdivision of icosahedron / octahedron / tetrahedron).
 * \author Jaroslav Sloup, Tomas Barak
 */

#include <cmath>
#include <iostream>
#include <string>

#include "trackball.h"
#include "pgr.h"

#if _MSC_VER
#define snprintf _snprintf
#endif

// vertices of icosahedron (polyhedron with 20 faces and 12 vertices), each face is an equilateral triangle
#define PX 0.525731112119133606
#define PZ 0.850650808352039932

float icosahedronVrts[] = {
  -PX, 0.0f,   PZ,
  PX, 0.0f,   PZ,
  -PX, 0.0f,  -PZ,
  PX, 0.0f,  -PZ,
  0.0f,   PZ,   PX,
  0.0f,   PZ,  -PX,
  0.0f,  -PZ,   PX,
  0.0f,  -PZ,  -PX,
  PZ,   PX, 0.0f,
  -PZ,   PX, 0.0f,
  PZ,  -PX, 0.0f,
  -PZ,  -PX, 0.0f
};

#undef PX
#undef PY

// indices of the icosahedron 20 faces
unsigned int icosahedronIndx[] = {
  1,  4,  0,
  4,  9,  0,
  4,  5,  9,
  8,  5,  4,
  1,  8,  4,
  1, 10,  8,
  10,  3,  8,
  8,  3,  5,
  3,  2,  5,
  3,  7,  2,
  3, 10,  7,
  10,  6,  7,
  6, 11,  7,
  6,  0, 11,
  6,  1,  0,
  10,  1,  6,
  11,  0,  9,
  2, 11,  9,
  5,  2,  9,
  11,  2,  7
};

const int icosahedronVerticesCount = 12;
const int icosahedronTrianglesCount = 20;

// vertices of octahedron (polyhedron with 8 faces and 6 vertices), each face is an equilateral triangle
float octahedronVrts[] = {
  1.0,  0.0,  0.0,
  0.0,  0.0, -1.0,
  -1.0,  0.0,  0.0,
  0.0,  0.0,  1.0,
  0.0, -1.0,  0.0,
  0.0,  1.0,  0.0
};

// indices of the octahedron 6 faces
unsigned int octahedronIndx[] = {
  3, 0, 5,
  2, 3, 5,
  3, 4, 0,
  2, 4, 3,
  0, 1, 5,
  1, 2, 5,
  4, 2, 1,
  0, 4, 1
};

const int octahedronVerticesCount = 6;
const int octahedronTrianglesCount = 8;

// vertices of tetrahedron (polyhedron with 4 faces and 4 vertices)
// each face is an equilateral triangle, edge length 2.0
const float invSqrt2 = (float)(1.0 / sqrt(2.0));
float tetrahedronVrts[] = {
  1.0f,  0.0f, -invSqrt2,
  0.0f,  1.0f,  invSqrt2,
  -1.0f,  0.0f, -invSqrt2,
  0.0f, -1.0f,  invSqrt2
};

// indices of the 6 faces
unsigned int tetrahedronIndx[] = {
  3, 0, 1,
  2, 3, 1,
  0, 2, 1,
  2, 0, 3
};

const int tetrahedronVerticesCount = 4;
const int tetrahedronTrianglesCount = 4;

int initialVerticesCount = icosahedronVerticesCount;
int initialTrianglesCount = icosahedronTrianglesCount;
float* initialSphereVertices = icosahedronVrts;
unsigned int* initialSphereIndices = icosahedronIndx;

const int WIN_WIDTH = 500;
const int WIN_HEIGHT = 500;
const char* WIN_TITLE = "PGR - Primitives - sphere";
const unsigned int REFRESH_INTERVAL = 33;
const int NLIGHTS = 3;
float elapsedTime = 0.0f;
glm::mat4 projection = glm::mat4(1.0f); // projection matrix
glm::mat4 view = glm::mat4(1.0f);       // viewing/camera matrix
glm::mat4 model;                        // modeling matrix

// virtual trackball related stuff
pgr::CQuaternionTrackball trackball;	// trackball class -> uses quaterninons to rotate the scene
int startGrabX, startGrabY;				// trackball starting point
int endGrabX, endGrabY;					// trackball end point
int winWidth, winHeight;				// window width & height
glm::mat4 trackballRotation;			// trackball rotation matrix

GLuint sphereShaderProgram = 0;
GLuint sphereVbo = 0;
GLuint sphereTrianglesEao = 0;
GLuint sphereTrianglesVao = 0;
GLenum displayMode = GL_LINE;

const float sphereRadius = 1.75f;
const float generatedSphereRadius = 1.0f;
float sphereCenterOff = -5.0f;
int subdivisionDepth = 2;
const int maxSubdivisionDepth = 9;
const int minSubdivisionDepth = 1;

// array of sphere vertices
unsigned int nSphereVertices = 0;
const unsigned nSphereAttribsPerVertex = 6; // x,y,z position, nx,ny,nz normal vector
float* sphereVertices = NULL;

// indices used to draw cube as GL_TRIANGLE_STRIP
unsigned nSphereTriangleIdx = 0;
unsigned int* sphereTriangleIdx = NULL;

GLint vertexLoc = -1;
GLint normalLoc = -1;
GLint colorLoc = -1;

glm::vec3 sphereColor(1.0f, 1.0f, 1.0f);

struct Light {

  Light(float r = 1.0f, float g = 1.0f, float b = 1.0f):
    color(r, g, b, 0.0f), position(0.0f, 0.0f, 0.0f, 1.0f) {}

  glm::vec4 position;
  glm::vec4 color;
};

Light lights[NLIGHTS] = {
  Light(1.0f, 0.0f, 0.0f),
  Light(0.0f, 1.0f, 0.0f),
  Light(0.0f, 0.0f, 1.0f)
};

// vertex shader just sends position and normal to the fragment shader
std::string vertexShaderSrc =
    "#version 140\n"
    "uniform mat4 modelMat;\n"
    "uniform mat4 projMat;\n"
    "in vec3 vertex;\n"
    "in vec3 normal;\n"
    "out vec3 normal_v;\n"
    "out vec3 position_v;\n"
    "void main()\n"
    "{\n"
    "  vec4 nor4 = modelMat * vec4(normal, 0.0f);\n"
    "  normal_v = nor4.xyz;\n"
    "  vec4 pos4 = modelMat * vec4(vertex, 1.0f);\n"
    "  position_v = pos4.xyz / pos4.w;\n"
    "  gl_Position = projMat * pos4;\n"
    "}\n"
    ;

// takes interpolated normal and position and calculates per pixel diffuse lighting
std::string fragmentShaderSrc =
    "#version 140\n"
    "#define NLIGHTS 3\n"
    "struct Light {\n"
    "  vec3 position;\n"
    "  vec3 color;\n"
    "};\n"
    "uniform Light lights[NLIGHTS];\n"
    "in vec3 normal_v;\n"
    "in vec3 position_v;\n"
    "uniform vec3 color;\n"
    "out vec3 color_f;\n"
    "void main()\n"
    "{\n"
    "  vec3 diffuse = color;\n"
    "  vec3 color = vec3(0.0f);\n"
    "  vec3 N = normalize(normal_v);\n"
    "  for(int l = 0; l < NLIGHTS; ++l) {\n"
    "    vec3 L = lights[l].position - position_v;\n"
    "    float dist = length(L);\n"
    "    float attenuation = min(1.0f / (dist * dist), 1.0f);\n"
    "    color += diffuse * lights[l].color * max(dot(N, L) / dist, 0.0f) * attenuation;\n"
    "  }\n"
    "  color_f = 0.85*color + vec3(0.15f, 0.15f, 0.15f);\n"
    "}\n"
    ;

// recurcive subdivision of icosahedron/octahedron/tetrahedron (depth is depth of subdivision)
// each equilateral triangle is subdivided into four triangles
void generateSphere(float radius, int depth) {
  int verticesCount = initialVerticesCount;
  int trianglesCount = initialTrianglesCount;

  int numTriagles = initialTrianglesCount;

  nSphereVertices = initialVerticesCount;

  for(int i=1; i<depth; i++) {
    nSphereVertices += 3 * numTriagles;
    numTriagles *= 4;
  }

  nSphereTriangleIdx = numTriagles * 3;

  if(sphereVertices != NULL) {
    delete[] sphereVertices;
    sphereVertices = NULL;
  }
  sphereVertices = new float[nSphereVertices * nSphereAttribsPerVertex];
  if(sphereTriangleIdx != NULL) {
    delete[] sphereTriangleIdx;
    sphereTriangleIdx = NULL;
  }
  sphereTriangleIdx = new unsigned int[nSphereTriangleIdx];

  float vx, vy, vz, len;
  int index;

  // copy initial vertices
  for(int i=0; i<initialVerticesCount; i++) {
    index = i*nSphereAttribsPerVertex;
    // copy vertex coordinates
    vx = initialSphereVertices[i*3+0];
    vy = initialSphereVertices[i*3+1];
    vz = initialSphereVertices[i*3+2];

    len = (float) (1.0 / sqrt(vx*vx + vy*vy + vz*vz));
    vx *= len; vy *= len; vz *= len;

    sphereVertices[index+0] = radius * vx;
    sphereVertices[index+1] = radius * vy;
    sphereVertices[index+2] = radius * vz;

    // store normal vector
    sphereVertices[index+3] = vx;
    sphereVertices[index+4] = vy;
    sphereVertices[index+5] = vz;
  }

  // copy initial face indices
  for(int i=0; i<initialTrianglesCount*3; i++)
    sphereTriangleIdx[i] = initialSphereIndices[i];

  if(depth != 1) {

    unsigned int *tmp = new unsigned int[nSphereTriangleIdx];

    float vx, vy, vz, len;
    int triPnt, indx1, indx2, indx3, trngls, i1, i2, i3;
    unsigned int *tmpPtr;

    /* Subdivide each triangle in the old approximation and place
     * the newly generated points to lie on the surface of the sphere.
     * Each input triangle with vertices labelled [1,2,3] as shown
     * below will be turned into four new triangles:
     *
     *                step 1) Make new points:
     *                   a = (1+2)/2
     *                   b = (2+3)/2
     *                   c = (1+3)/2
     *	      3
     *       /\       step 2) Normalize a, b, c and multiply by the sphere radius
     *      /  \
     *    c/____\b    step 3) Construct four new triangles:
     *    /\    /\		    [1,a,c]
     *   /	\  /  \		    [2,b,a]
     *  /____\/____\	    [3,c,b]
     * 1     a      2	    [c,a,b]
     *
     */

    // recurcive subdivision
    for(int sub=1; sub<depth; sub++) {

      triPnt = 0;
      trngls = 0;
      for(int tri=0; tri<trianglesCount; tri++) {

        index = 3 * tri;
        i1 = sphereTriangleIdx[index + 0]; // triangle vertex 1 index
        i2 = sphereTriangleIdx[index + 1]; // triangle vertex 2 index
        i3 = sphereTriangleIdx[index + 2]; // triangle vertex 3 index
        indx1 = i1 * nSphereAttribsPerVertex;
        indx2 = i2 * nSphereAttribsPerVertex;
        indx3 = i3 * nSphereAttribsPerVertex;

        // new vertex - in the middle of edge V1V2
        vx = 0.5f*(sphereVertices[indx1 + 0] + sphereVertices[indx2 + 0]);
        vy = 0.5f*(sphereVertices[indx1 + 1] + sphereVertices[indx2 + 1]);
        vz = 0.5f*(sphereVertices[indx1 + 2] + sphereVertices[indx2 + 2]);
        len = (float) (1.0 / sqrt(vx*vx + vy*vy + vz*vz));
        vx *= len; vy *= len; vz *= len;

        // increase number of vertices and add a new vertex
        index = nSphereAttribsPerVertex * verticesCount;
        sphereVertices[index + 3] = vx;
        sphereVertices[index + 4] = vy;
        sphereVertices[index + 5] = vz;
        vx *= radius; vy *= radius; vz *= radius;
        sphereVertices[index + 0] = vx;
        sphereVertices[index + 1] = vy;
        sphereVertices[index + 2] = vz;
        verticesCount++;

        // new vertex - in the middle of edge V2V3
        vx = 0.5f*(sphereVertices[indx2 + 0] + sphereVertices[indx3 + 0]);
        vy = 0.5f*(sphereVertices[indx2 + 1] + sphereVertices[indx3 + 1]);
        vz = 0.5f*(sphereVertices[indx2 + 2] + sphereVertices[indx3 + 2]);
        len = (float) (1.0 / sqrt(vx*vx + vy*vy + vz*vz));
        vx *= len; vy *= len; vz *= len;

        // increase number of vertices and add a new vertex
        index = nSphereAttribsPerVertex * verticesCount;
        sphereVertices[index + 3] = vx;
        sphereVertices[index + 4] = vy;
        sphereVertices[index + 5] = vz;
        vx *= radius; vy *= radius; vz *= radius;
        sphereVertices[index + 0] = vx;
        sphereVertices[index + 1] = vy;
        sphereVertices[index + 2] = vz;
        verticesCount++;

        // new vertex - in the middle of edge V1V3
        vx = 0.5f*(sphereVertices[indx3 + 0] + sphereVertices[indx1 + 0]);
        vy = 0.5f*(sphereVertices[indx3 + 1] + sphereVertices[indx1 + 1]);
        vz = 0.5f*(sphereVertices[indx3 + 2] + sphereVertices[indx1 + 2]);
        len = (float) (1.0 / sqrt(vx*vx + vy*vy + vz*vz));
        vx *= len; vy *= len; vz *= len;

        // increase number of vertices and add a new vertex
        index = nSphereAttribsPerVertex * verticesCount;
        sphereVertices[index + 3] = vx;
        sphereVertices[index + 4] = vy;
        sphereVertices[index + 5] = vz;
        vx *= radius; vy *= radius; vz *= radius;
        sphereVertices[index + 0] = vx;
        sphereVertices[index + 1] = vy;
        sphereVertices[index + 2] = vz;
        verticesCount++;

        // add four new triangles
        tmp[triPnt++] = i1;
        tmp[triPnt++] = verticesCount-3;
        tmp[triPnt++] = verticesCount-1;

        tmp[triPnt++] = i2;
        tmp[triPnt++] = verticesCount-2;
        tmp[triPnt++] = verticesCount-3;

        tmp[triPnt++] = i3;
        tmp[triPnt++] = verticesCount-1;
        tmp[triPnt++] = verticesCount-2;

        tmp[triPnt++] = verticesCount-1;
        tmp[triPnt++] = verticesCount-3;
        tmp[triPnt++] = verticesCount-2;

        trngls += 4;
      }

      tmpPtr = sphereTriangleIdx;
      sphereTriangleIdx = tmp;
      tmp = tmpPtr;

      trianglesCount = trngls;
    }
    delete[] tmp;
  }

  std::cout << "Sphere regenerated:" << std::endl;
  std::cout << "* number of vertices:" << verticesCount << std::endl;
  std::cout << "* number of triangles:" << trianglesCount << std::endl;
  std::cout << "* subdivision depth:" << subdivisionDepth << std::endl;
  std::cout << std::endl;
}

void updateBuffers(void) {
  glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
  glBufferData(GL_ARRAY_BUFFER, nSphereVertices*nSphereAttribsPerVertex*sizeof(float), sphereVertices, GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // buffer for triangle indices
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sphereTrianglesEao);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, nSphereTriangleIdx*sizeof(unsigned int), sphereTriangleIdx, GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void init() {

  // opengl setup
  glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glPointSize(5.0f);

  // load shader
  std::vector<GLuint> shaders;
  shaders.push_back(pgr::createShaderFromSource(GL_VERTEX_SHADER, vertexShaderSrc));
  shaders.push_back(pgr::createShaderFromSource(GL_FRAGMENT_SHADER, fragmentShaderSrc));
  sphereShaderProgram = pgr::createProgram(shaders);

  // handles to vertex shader inputs
  vertexLoc = glGetAttribLocation(sphereShaderProgram, "vertex");
  normalLoc = glGetAttribLocation(sphereShaderProgram, "normal");
  colorLoc = glGetUniformLocation(sphereShaderProgram, "color");

  generateSphere(generatedSphereRadius, subdivisionDepth);

  // buffer for vertices
  glGenBuffers(1, &sphereVbo);

  // buffer for triangle indices
  glGenBuffers(1, &sphereTrianglesEao);

  updateBuffers();

  // indexed triangle strips
  glGenVertexArrays(1, &sphereTrianglesVao);
  glBindVertexArray(sphereTrianglesVao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sphereTrianglesEao);
  glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
  glEnableVertexAttribArray(vertexLoc);
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, nSphereAttribsPerVertex * sizeof(float), (void *)(0));
  glEnableVertexAttribArray(normalLoc);
  glVertexAttribPointer(normalLoc, 3, GL_FLOAT, GL_FALSE, nSphereAttribsPerVertex * sizeof(float), (void *)(nSphereAttribsPerVertex/2 * sizeof(float)));

  glBindVertexArray(0);
  CHECK_GL_ERROR();

  // initialize trackball -> drag from point [0,0] to point [0,0]
  trackball.setRotation(0.0f, 0.0f, 0.0f, 0.0f);
  // viewing matrix
  view = glm::lookAt( glm::vec3(0.0f, 0.0f, -sphereCenterOff), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f) );
  // modeling matrix
  model = glm::scale(glm::mat4(1.0f), glm::vec3(sphereRadius, sphereRadius, sphereRadius));
}

// deletes allocated buffers
void cleanup() {

  glDeleteVertexArrays(1, &sphereTrianglesVao);
  glDeleteBuffers(1, &sphereTrianglesEao);
  glDeleteBuffers(1, &sphereVbo);
  pgr::deleteProgramAndShaders(sphereShaderProgram);

  if(sphereVertices != NULL) {
    delete[] sphereVertices;
    sphereVertices = NULL;
  }
  if(sphereTriangleIdx != NULL) {
    delete[] sphereTriangleIdx;
    sphereTriangleIdx = NULL;
  }
}

void switchMode() {

  switch(displayMode) {
    case GL_POINT:
      displayMode = GL_LINE;
      break;
    case GL_LINE:
      displayMode = GL_FILL;
      break;
    case GL_FILL:
      displayMode = GL_POINT;
      break;
    default:;
  }
}

void refreshCb(int) {

  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);
  elapsedTime = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME);	// milliseconds => seconds

  float rotAngleDeg = elapsedTime * 10.0f;	 // rotate 10 degrees per second
  float l0rotAngleDeg = elapsedTime * 60.0f; // rotate 60 degrees per second
  float l1rotAngleDeg = elapsedTime * 40.0f; // rotate 40 degrees per second
  float radius = 2.5f;

  lights[0].position = glm::vec4(radius * cos(glm::radians(l0rotAngleDeg)), radius * sin(glm::radians(l0rotAngleDeg)), sphereCenterOff, 1.0f);
  lights[1].position = glm::vec4(radius * cos(glm::radians(l1rotAngleDeg)), 0.0f, radius * sin(glm::radians(l1rotAngleDeg)) + sphereCenterOff, 1.0f);
  lights[2].position = glm::vec4(0.0f, radius * cos(glm::radians(l1rotAngleDeg)), radius * sin(glm::radians(l1rotAngleDeg)) + sphereCenterOff, 1.0f);

  model = glm::rotate(glm::mat4(1.0f), rotAngleDeg, glm::vec3(0.0f, 1.0f, 0.0f)); // the animation - model rotation
  model = glm::scale(model, glm::vec3(sphereRadius, sphereRadius, sphereRadius));

  glutPostRedisplay();
}

void displayCb() {

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // compose modeling and viewing transformations
  glm::mat4 newModelView = view * trackballRotation * model;

  glUseProgram(sphereShaderProgram);
  glUniformMatrix4fv(glGetUniformLocation(sphereShaderProgram, "modelMat"), 1, GL_FALSE, glm::value_ptr(newModelView));
  glUniformMatrix4fv(glGetUniformLocation(sphereShaderProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(projection));

  for(int light = 0; light < NLIGHTS; ++light) {
    char buf[255];
    snprintf(buf, 255, "lights[%i].position", light);
    glUniform3fv(glGetUniformLocation(sphereShaderProgram, buf), 1, glm::value_ptr(lights[light].position));
    snprintf(buf, 255, "lights[%i].color", light);
    glUniform3fv(glGetUniformLocation(sphereShaderProgram, buf), 1, glm::value_ptr(lights[light].color));
  }

  glPolygonMode(GL_FRONT_AND_BACK, displayMode);

  glBindVertexArray(sphereTrianglesVao);

  glUniform3fv(colorLoc, 1, glm::value_ptr(sphereColor));
  glDrawElements(GL_TRIANGLES, nSphereTriangleIdx, GL_UNSIGNED_INT, (GLvoid*)(0));

  glBindVertexArray( 0 );

  glutSwapBuffers();
}

void reshapeCb(int w, int h) {

  winWidth = w;
  winHeight = h;

  glViewport(0, 0, w, h);
  projection =  glm::perspective(60.0f, float(w) / float(h) , 1.0f, 10.0f);
}

void keyboardCb(unsigned char key, int x, int y) {

  switch (key) {
    case 27:
      glutLeaveMainLoop();
      break;
    case ' ':
      switchMode();
      break;
  }
}

void specialCb(int key, int x, int y) {

  switch (key) {
    case GLUT_KEY_LEFT:
      if(subdivisionDepth > minSubdivisionDepth) {
        subdivisionDepth--;
        generateSphere(generatedSphereRadius, subdivisionDepth);
        updateBuffers();
      }
      break;
    case GLUT_KEY_RIGHT:
      if(subdivisionDepth < maxSubdivisionDepth) {
        subdivisionDepth++;
        generateSphere(generatedSphereRadius, subdivisionDepth);
        updateBuffers();
      }
      break;
  }
  glutPostRedisplay();
}


void passiveMouseCb(int x, int y) {

  endGrabX = x;
  endGrabY = y;

  if(startGrabX != endGrabX || startGrabY != endGrabY) {

    /* get rotation from trackball using quaternion */
    trackball.addRotation(startGrabX, startGrabY, endGrabX, endGrabY, winWidth, winHeight);

    /* build rotation matrix from quaternion */
    trackball.getRotationMatrix(trackballRotation);

    startGrabX = endGrabX;
    startGrabY = endGrabY;

    glutPostRedisplay();
  }
}

void mouseCb(int button, int state, int x, int y) {

  if(button == GLUT_LEFT_BUTTON) {
    if(state == GLUT_DOWN) {
      startGrabX = x;
      startGrabY = y;
      glutMotionFunc(passiveMouseCb);
      return;
    } else {
      glutMotionFunc(NULL);
    }
  }
}

void menuCb(int menuItem) {
  switch(menuItem) {
    case 1:
      initialVerticesCount = tetrahedronVerticesCount;
      initialTrianglesCount = tetrahedronTrianglesCount;
      initialSphereVertices = tetrahedronVrts;
      initialSphereIndices = tetrahedronIndx;
      break;
    case 2:
      initialVerticesCount = octahedronVerticesCount;
      initialTrianglesCount = octahedronTrianglesCount;
      initialSphereVertices = octahedronVrts;
      initialSphereIndices = octahedronIndx;
      break;
    case 3:
      initialVerticesCount = icosahedronVerticesCount;
      initialTrianglesCount = icosahedronTrianglesCount;
      initialSphereVertices = icosahedronVrts;
      initialSphereIndices = icosahedronIndx;
      break;
    case 4:
      cleanup();
      glutLeaveMainLoop();
      break;
    default:
      return;
  }
  generateSphere(generatedSphereRadius, subdivisionDepth);
  updateBuffers();
  glutPostRedisplay();
}

int main(int argc, char** argv) {

  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow(WIN_TITLE);

  int subMenu = glutCreateMenu(menuCb);
  glutAddMenuEntry("Tetrahedron", 1);
  glutAddMenuEntry("Octahedron", 2);
  glutAddMenuEntry("Icosahedron", 3);
  glutCreateMenu(menuCb);
  glutAddSubMenu("Sphere initial shape", subMenu);
  glutAddMenuEntry("Quit", 4);
  glutAttachMenu(GLUT_RIGHT_BUTTON);

  glutDisplayFunc(displayCb);
  glutReshapeFunc(reshapeCb);
  glutKeyboardFunc(keyboardCb);
  glutSpecialFunc(specialCb);
  glutMouseFunc(mouseCb);
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);

  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  init();

  std::cout << "Use space to cycle through display modes, Esc to quit." << std::endl;
  std::cout << "Use left and right arrows to increase / decrease sphere subdivision depth." << std::endl;
  std::cout << "Select initial sphere shape by menu." << std::endl;

  glutMainLoop();
  cleanup();
  return 0;
}
