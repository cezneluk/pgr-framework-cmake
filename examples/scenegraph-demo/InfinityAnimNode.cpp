//----------------------------------------------------------------------------------------
/**
 * \file    InfinityAnimNode.cpp
 * \author  Jaroslav Sloup, Tomas Barak
 * \date    2013
 * \brief   Simple animation node based on object movement along a curve with infinity sign like shape.
 */
//----------------------------------------------------------------------------------------

#include "pgr.h"                  // includes all PGR libraries, like shader, glm, assimp ...
#include "InfinityAnimNode.h"

InfinityAnimNode::InfinityAnimNode(const char* name, pgr::sg::SceneNode* parent):
  pgr::sg::SceneNode(name, parent), m_axis(1, 0, 0), m_angle(0), m_speed(0) {
}

void InfinityAnimNode::update(double elapsedTime) {
  const float CIRCLE_RADIUS = 2.5f;

  m_angle = m_speed * (float) elapsedTime;

  int phase = int(m_angle / (M_PI * 2.0f));
  glm::vec3 position = glm::vec3(CIRCLE_RADIUS - CIRCLE_RADIUS*cos(m_angle), 0.0f, -CIRCLE_RADIUS*sin(m_angle));

  if(phase % 2 == 1) {
    position.x = -position.x;
    m_angle = -m_angle;
  }

  m_local_mat = glm::translate(glm::mat4(1.0f), position);
  m_local_mat = glm::rotate(m_local_mat, 180-glm::degrees(m_angle), m_axis);

  /// call inherited update (which calculates global matrix and updates children)
  pgr::sg::SceneNode::update(elapsedTime);
}
