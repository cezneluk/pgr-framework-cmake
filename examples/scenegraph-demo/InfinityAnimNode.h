//----------------------------------------------------------------------------------------
/**
 * \file    InfinityAnimNode.h
 * \author  Jaroslav Sloup, Tomas Barak
 * \date    2013
 * \brief   Simple animation node based on object movement along a curve with infinity sign like shape.
 */
//----------------------------------------------------------------------------------------

#ifndef __INFINITY_ANIM_NODE_H
#define __INFINITY_ANIM_NODE_H

#include "sceneGraph/SceneNode.h"

class InfinityAnimNode : public pgr::sg::SceneNode {

public:
  InfinityAnimNode(const char* name = "InfinityAnimNode", pgr::sg::SceneNode* parent = NULL);
  ~InfinityAnimNode() {}

  void setSpeed(float speed) {
    m_speed = speed;
  }

  void setAxis(const glm::vec3 & axis) {
    m_axis = axis;
  }

  void update(double elapsedTime);

protected:

  glm::vec3 m_axis;
  float     m_angle;
  float     m_speed;
};

#endif // __INFINITY_ANIM_NODE_H
