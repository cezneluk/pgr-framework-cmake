//----------------------------------------------------------------------------------------
/**
 * \file    ScaleAnimNode.h
 * \author  Jaroslav Sloup, Tomas Barak
 * \date    2013
 * \brief   Simple animation node based on object scaling.
 */
//----------------------------------------------------------------------------------------

#ifndef __SCALE_ANIM_NODE_H
#define __SCALE_ANIM_NODE_H

#include "sceneGraph/SceneNode.h"

/// simple scaling animation
class ScaleAnimNode : public pgr::sg::SceneNode {

public:

  ScaleAnimNode(const std::string& name = "ScaleAnimNode", pgr::sg::SceneNode* parent = NULL);
  ~ScaleAnimNode() {}

  /// Sets the speed of scaling.
  void setSpeed(float speed) {
    m_speed = speed;
  }

  /// Sets scaling bounds.
  void setRange(const float minScale, const float maxScale) {
    m_minScale = minScale;
    m_maxScale = maxScale;
    m_scaleRange = m_maxScale - m_minScale;
  }

  void update(double elapsedTime);

protected:

  float m_speed;
  float m_minScale;
  float m_maxScale;
  float m_scaleRange;
};

#endif // __SCALE_ANIM_NODE_H
