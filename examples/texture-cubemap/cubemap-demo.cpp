/**
 * \file cubemap-demo.cpp
 * \brief Displays a cube with lights orbiting (per pixel lighting).
 * \author Tomas Barak & Jaroslav Sloup
 */

#include <cmath>
#include <iostream>
#include <string>
#include <stdio.h>

#include "trackball.h"
#include "pgr.h"

#if _MSC_VER
#define snprintf _snprintf
#endif

const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 500;
const char * WIN_TITLE = "Environment mapping demo - Cube map ";
const unsigned int REFRESH_INTERVAL = 33;
const int NLIGHTS = 3;
double elapsedTime = 0.0;
glm::mat4 projection = glm::mat4(1.0f);
glm::mat4 model = glm::mat4(1.0f);

GLuint cubeShaderProgram = 0;
GLuint cubeVbo = 0;
GLuint cubeTrianglesEao = 0;
GLuint cubeTrianglesVao = 0;

GLuint pointsVao = 0;
GLuint pointsVbo = 0;
GLuint pointsShaderProgram = 0;

GLuint texID = 0;

pgr::CQuaternionTrackball trackball;					// trackball class -> uses quaterninons to rotate the scene
int startGrabX, startGrabY;						// trackball starting point
int endGrabX, endGrabY;							// trackball end point
int winWidth, winHeight;						// window width & height
glm::mat4 trackballRotation = glm::mat4(1.0f);	// trackball rotation matrix
glm::mat4 modelRotation = glm::mat4(1.0f);		// model rotation matrix

// SELECT MODEL
const pgr::MeshData & meshData = pgr::teapotData;
//const pgr::MeshData & meshData = pgr::monkeyData;
//const pgr::MeshData & meshData = pgr::cubeData;

// SELECT TEXTURE
const char * CUBE_TEXTURE_FILE_PREFIX = "textures/SaintLazarusChurch";
//const char * CUBE_TEXTURE_FILE_PREFIX = "textures/cm";  //  textures from nVidia demo

struct Light {
  Light(float r = 1.0f, float g = 1.0f, float b = 1.0f):
    color(r, g, b, 0.0f), position(0.0f, 0.0f, 0.0f, 1.0f) {}

  glm::vec4 position;
  glm::vec4 color;
};

Light lights[NLIGHTS] = {
  Light(1.0f, 0.0f, 0.0f),
  Light(0.0f, 1.0f, 0.0f),
  Light(0.0f, 0.0f, 1.0f)
};

// vertex shader just sends position and normal to the fragment shader
std::string cubeVertexShaderSrc =
    "#version 140\n"
    "uniform mat4 modelMat;\n"
    "uniform mat4 projMat;\n"
    "in vec3 vertex;\n"
    "in vec3 normal;\n"
    "noperspective out vec3 reflectDir;\n"
    "uniform vec3 worldCameraPosition;\n"
    "out vec3 normal_v;\n"
    "out vec3 position_v;\n"
    "void main()\n"
    "{\n"
    "  vec4 nor4 = modelMat * vec4(normal, 0.0f);\n"
    "  normal_v = nor4.xyz;\n"
    "  vec4 pos4 = modelMat * vec4(vertex, 1.0f);\n"
    "  position_v = pos4.xyz / pos4.w;\n"
    "  vec3 worldView = normalize( worldCameraPosition - position_v );\n"
    "  reflectDir = reflect(-worldView, normal_v );\n"
    "  gl_Position = projMat * pos4;\n"
    "}\n"
    ;

// takes interpolated normal and position and calculates per pixel diffuse lighting
std::string cubeFragmentShaderSrc =
    "#version 140\n"
    "\n"
    "noperspective in vec3 reflectDir;\n"
    "uniform samplerCube cubeMapTex;\n"
    "uniform float reflectFactor;\n"
    "\n"
    "#define NLIGHTS 3\n"
    "struct Light {\n"
    "  vec3 position;\n"
    "  vec3 color;\n"
    "};\n"
    "uniform Light lights[NLIGHTS];\n"
    "in vec3 normal_v;\n"
    "in vec3 position_v;\n"
    "out vec4 color_f;\n"
    "\n"
    "void main() {\n"
    "  vec3 diffuse = vec3(1.0f);\n"
    "  vec3 color = vec3(0.0f);\n"
    "  vec3 N = normalize(normal_v);\n"
    "  for(int l = 0; l < NLIGHTS; ++l) {\n"
    "    vec3 L = lights[l].position - position_v;\n"
    "    float dist = length(L);\n"
    "    float attenuation = min(1.0f / (dist * dist), 1.0f);\n"
    "    color += diffuse * lights[l].color * max(dot(N, L) / dist, 0.0f) * attenuation;\n"
    "  }\n"
    "\n"
    "  vec4 cubeMapColor = texture(cubeMapTex, reflectDir);\n"
    "  color_f = mix( vec4(color, 1.0), cubeMapColor, reflectFactor);\n"
    "}\n"
    ;

// vertex shader just sends the color to the FS and translates the position
std::string pointsVertexShaderSrc =
    "#version 140\n"
    "uniform mat4 modelMat;\n"
    "uniform mat4 projMat;\n"
    "in vec4 position;\n"
    "in vec4 color;\n"
    "out vec4 color_v;\n"
    "void main()\n"
    "{\n"
    "  gl_Position = projMat * position;\n"
    "  color_v = color;\n"
    "}\n"
    ;

// only writes interpolated color
std::string pointsFragmentShaderSrc =
    "#version 140\n"
    "in vec4 color_v;\n"
    "out vec3 color_f;\n"
    "void main()\n"
    "{\n"
    "  color_f = color_v.rgb;\n"
    "}\n"
    ;

void loadCubeMap( const char * baseFileName ) {

  glActiveTexture(GL_TEXTURE0);

  glGenTextures(1, &texID);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texID);

  const char * suffixes[] = { "posx", "negx", "posy", "negy", "posz", "negz" };
  GLuint targets[] = {
    GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
  };

  for( int i = 0; i < 6; i++ ) {
    std::string texName = std::string(baseFileName) + "_" + suffixes[i] + ".jpg";
    std::cout << "Loading: " << texName << std::endl;
    if(!pgr::loadTexImage2D(texName, targets[i])) {
      std::cerr << __FUNCTION__ << " cannot load image " << texName << std::endl;
      return;
    }
  }

  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

  // unbind the texture (just in case someone will mess up with texture calls later)
  glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
  CHECK_GL_ERROR();

  GLint cubeMapTexLoc= glGetUniformLocation(cubeShaderProgram, "cubeMapTex");
  GLint worldCameraPositionLoc = glGetUniformLocation(cubeShaderProgram, "worldCameraPosition");
  GLint reflectFactorLoc = glGetUniformLocation(cubeShaderProgram, "reflectFactor");

  glUseProgram(cubeShaderProgram);

  glUniform1i(cubeMapTexLoc, 0);
  glUniform3fv(worldCameraPositionLoc, 1, glm::value_ptr(glm::vec3(0.0f, 0.0f, 0.0f)));
  glUniform1f( reflectFactorLoc, 0.75f);

  CHECK_GL_ERROR();
}


void init() {
  // opengl setup
  glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glPointSize(5.0f);

  // load shaders
  GLuint cubeShaders [] = {
    pgr::createShaderFromSource(GL_VERTEX_SHADER, cubeVertexShaderSrc),
    pgr::createShaderFromSource(GL_FRAGMENT_SHADER, cubeFragmentShaderSrc),
    0
  };
  cubeShaderProgram = pgr::createProgram(cubeShaders);

  // handles to vertex shader inputs
  GLint cubeVertexLoc = glGetAttribLocation(cubeShaderProgram, "vertex");
  GLint cubeNormalLoc = glGetAttribLocation(cubeShaderProgram, "normal");

  GLuint pointsShaders [] = {
    pgr::createShaderFromSource(GL_VERTEX_SHADER, pointsVertexShaderSrc),
    pgr::createShaderFromSource(GL_FRAGMENT_SHADER, pointsFragmentShaderSrc),
    0
  };
  pointsShaderProgram = pgr::createProgram(pointsShaders);

  // handles to vertex shader inputs
  GLint pointsVertexLoc = glGetAttribLocation(pointsShaderProgram, "position");
  GLint pointsColorLoc = glGetAttribLocation(pointsShaderProgram, "color");

  // buffer for vertices
  glGenBuffers(1, &cubeVbo);
  glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * meshData.nVertices * meshData.nAttribsPerVertex, meshData.verticesInterleaved, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // buffer for triangle indices
  glGenBuffers(1, &cubeTrianglesEao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeTrianglesEao);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * meshData.nTriangles * 3, meshData.triangles, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  // indexed triangles
  glGenVertexArrays(1, &cubeTrianglesVao);
  glBindVertexArray(cubeTrianglesVao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeTrianglesEao);
  glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);
  glEnableVertexAttribArray(cubeVertexLoc);
  glEnableVertexAttribArray(cubeNormalLoc);
  glVertexAttribPointer(cubeVertexLoc, 3, GL_FLOAT, GL_FALSE, meshData.nAttribsPerVertex * sizeof(float), (void *)(0));
  glVertexAttribPointer(cubeNormalLoc, 3, GL_FLOAT, GL_FALSE, meshData.nAttribsPerVertex * sizeof(float), (void *)(3 * sizeof(float)));

  // buffer for light "dots" rendering
  glGenBuffers(1, &pointsVbo);
  glBindBuffer(GL_ARRAY_BUFFER, pointsVbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(lights), &lights, GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glGenVertexArrays(1, &pointsVao);
  glBindVertexArray(pointsVao);
  glBindBuffer(GL_ARRAY_BUFFER, pointsVbo);
  glEnableVertexAttribArray(pointsVertexLoc);
  glEnableVertexAttribArray(pointsColorLoc);
  glVertexAttribPointer(pointsVertexLoc, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(0));
  glVertexAttribPointer(pointsColorLoc, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(4 * sizeof(float)));

  glBindVertexArray(0);
  CHECK_GL_ERROR();

  loadCubeMap(CUBE_TEXTURE_FILE_PREFIX);

  // initialize trackball -> drag from point [0,0] to point [0,0]
  trackball.setRotation(0.0f, 0.0f, 0.0f, 0.0f);
}

// deletes allocated buffers
void cleanup() {
  glDeleteVertexArrays(1, &cubeTrianglesVao);
  glDeleteBuffers(1, &cubeTrianglesEao);
  glDeleteBuffers(1, &cubeVbo);
  glDeleteVertexArrays(1, &pointsVao);
  glDeleteBuffers(1, &pointsVbo);
  pgr::deleteProgramAndShaders(cubeShaderProgram);
  pgr::deleteProgramAndShaders(pointsShaderProgram);
}

void refreshCb(int) {
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);
  elapsedTime = 0.001 * (double)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

  // move cube 5 units back
  float cubeCenterOff = -4.0f;
  float radius = 2.5f;
  float cubeRotAngleDeg = (float) elapsedTime * 30.0f; // rotate 30 degrees per second
  float l0rotAngleDeg   = (float) elapsedTime * 60.0f; // rotate 60 degrees per second
  float l1rotAngleDeg   = (float) elapsedTime * 40.0f; // rotate 40 degrees per second

  lights[0].position = glm::vec4(radius * cos(glm::radians(l0rotAngleDeg)), radius * sin(glm::radians(l0rotAngleDeg)), cubeCenterOff, 1.0f);
  lights[1].position = glm::vec4(radius * cos(glm::radians(l1rotAngleDeg)), 0.0f, radius * sin(glm::radians(l1rotAngleDeg)) + cubeCenterOff, 1.0f);
  lights[2].position = glm::vec4(0.0f, radius * cos(glm::radians(l1rotAngleDeg)), radius * sin(glm::radians(l1rotAngleDeg)) + cubeCenterOff, 1.0f);

  model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, cubeCenterOff));
  model = glm::rotate(model, -30.0f, glm::vec3(1.0f, 0.0f, 0.0f)); // tilt cube by 30 degrees

  // the animation -> cube rotation
  modelRotation = glm::rotate(glm::mat4(1.0f), cubeRotAngleDeg * 0.5f, glm::vec3(0.0f, 1.0f, 0.0f)); // the animation

  glutPostRedisplay();
}

void displayCb() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // compose model transformations
  glm::mat4 newModel = model * trackballRotation * modelRotation;

  glUseProgram(cubeShaderProgram);
  glUniformMatrix4fv(glGetUniformLocation(cubeShaderProgram, "modelMat"), 1, GL_FALSE, glm::value_ptr(newModel));
  glUniformMatrix4fv(glGetUniformLocation(cubeShaderProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(projection));

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texID);

  for(int l = 0; l < NLIGHTS; ++l) {
    char buf[255];
    snprintf(buf, 255, "lights[%i].position", l);
    glUniform3fv(glGetUniformLocation(cubeShaderProgram, buf), 1, glm::value_ptr(lights[l].position));
    snprintf(buf, 255, "lights[%i].color", l);
    glUniform3fv(glGetUniformLocation(cubeShaderProgram, buf), 1, glm::value_ptr(lights[l].color));
  }

  glBindVertexArray(cubeTrianglesVao);
  glDrawElements(GL_TRIANGLES, meshData.nTriangles * 3, GL_UNSIGNED_INT, 0);

  glUseProgram(pointsShaderProgram);
  glUniformMatrix4fv(glGetUniformLocation(pointsShaderProgram, "modelMat"), 1, GL_FALSE, glm::value_ptr(model));
  glUniformMatrix4fv(glGetUniformLocation(pointsShaderProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(projection));
  glBindVertexArray(pointsVao);
  glBindBuffer(GL_ARRAY_BUFFER, pointsVbo);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(lights), &lights);
  glDrawArrays(GL_POINTS, 0, NLIGHTS);

  glutSwapBuffers();
}

void reshapeCb(int w, int h) {
  winWidth = w;
  winHeight = h;

  glViewport(0, 0, w, h);
  projection =  glm::perspective(60.0f, float(w) / float(h) , 1.0f, 10.0f);
}

void keyboardCb(unsigned char key, int x, int y) {
  switch (key) {
    case 27:
      glutLeaveMainLoop();
      break;
  }
}

void passiveMouseCb(int x, int y) {

  endGrabX = x;
  endGrabY = y;

  if(startGrabX != endGrabX || startGrabY != endGrabY) {

    /* get rotation from trackball using quaternion */
    trackball.addRotation(startGrabX, startGrabY, endGrabX, endGrabY, winWidth, winHeight);

    /* build rotation matrix from quaternion */
    trackball.getRotationMatrix(trackballRotation);

    startGrabX = endGrabX;
    startGrabY = endGrabY;

    glutPostRedisplay();
  }
}

void mouseCb(int button, int state, int x, int y) {

  if(button == GLUT_LEFT_BUTTON) {
    if(state == GLUT_DOWN) {
      startGrabX = x;
      startGrabY = y;
      glutMotionFunc(passiveMouseCb);
      return;
    } else {
      glutMotionFunc(NULL);
    }
  }
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow(WIN_TITLE);

  glutDisplayFunc(displayCb);
  glutReshapeFunc(reshapeCb);
  glutKeyboardFunc(keyboardCb);
  glutMouseFunc(mouseCb);
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);

  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  init();

  std::cout << "Three lights orbiting a cube. Press Esc to quit." << std::endl;

  glutMainLoop();
  cleanup();
  return 0;
}
