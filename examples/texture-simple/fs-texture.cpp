//----------------------------------------------------------------------------------------
/**
 * \file    fs-texture.cpp
 * \author  Tomas Barak
 * \date    2012/09/16
 * \brief   Simple fragment shader with texturing
 */
//----------------------------------------------------------------------------------------

#include <iostream>

#include "pgr.h"

const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 500;
const char * WIN_TITLE = "PGR - FS-texture";

struct State {
  float splitX;
} state;

struct Resources {
  GLuint program;
  GLuint vbo, vao;
  GLuint tex;
} resources;

struct Handles {
  GLint position;
  GLint splitX, tex;
} handles;

const char * srcVertexSh =
    "#version 140\n"
    "in vec2 aPosition;\n"
    "out vec2 vTexCoord;\n"
    "void main() {\n"
    "  vTexCoord = aPosition * 0.5f + vec2(0.5f);\n"
    "  gl_Position = vec4(aPosition, 0.0f, 1.0f);\n"
    "}\n"
    "\n";

const char * srcFragmentSh =
    "#version 140\n"
    "in vec2 vTexCoord;"
    "uniform sampler2D tex;\n"
    "uniform float splitX;\n"
    "out vec4 fColor;"
    "void main() {\n"
    "  vec4 color = texture(tex, vTexCoord);\n"
    "  if(vTexCoord.x > splitX) {\n"
    "    float luma = 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;\n"
    "    fColor = vec4(luma, luma, luma, 1.0f);\n"
    "  } else {\n"
    "    fColor = color;\n"
    "  }\n"
    "}\n"
    "\n";

void onReshape(int width, int height) {
  glViewport(0, 0, width, height);
}

void onDisplay() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(resources.program);
  glUniform1f(handles.splitX, state.splitX);

  glBindTexture(GL_TEXTURE_2D, resources.tex);
  glBindVertexArray(resources.vao);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glBindVertexArray(0);

  CHECK_GL_ERROR();
  glutSwapBuffers();
}

void onMouse(int, int, int x, int) {
  int width = glutGet(GLUT_WINDOW_WIDTH);
  state.splitX = float(x) / width;
  glutPostRedisplay();
}

void onMouseMotion(int x, int) {
  int width = glutGet(GLUT_WINDOW_WIDTH);
  state.splitX = float(x) / width;
  glutPostRedisplay();
}

bool init() {
  resources.tex = pgr::createTexture(pgr::frameworkRoot() + "data/nvidia-logo-small.jpg");
  if(resources.tex == 0)
    return false;

  std::vector<GLuint> shaders;
  shaders.push_back(pgr::createShaderFromSource(GL_VERTEX_SHADER, srcVertexSh));
  shaders.push_back(pgr::createShaderFromSource(GL_FRAGMENT_SHADER, srcFragmentSh));
  if(shaders.size() != 2)
    return false;
  resources.program = pgr::createProgram(shaders);
  if(resources.program == 0)
    return false;

  handles.position = glGetAttribLocation(resources.program, "aPosition");
  handles.tex = glGetUniformLocation(resources.program, "tex");
  handles.splitX = glGetUniformLocation(resources.program, "splitX");
  if(handles.position == -1 || handles.tex == -1 || handles.splitX == -1)
    return false;

  glUseProgram(resources.program);
  glUniform1i(handles.tex, 0); // 0th - 1st texture unit
  state.splitX = 0.5f;

  glGenVertexArrays(1, &resources.vao);
  glBindVertexArray(resources.vao);

  float positions[] = {-1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f};
  glGenBuffers(1, &resources.vbo);
  glBindBuffer(GL_ARRAY_BUFFER, resources.vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(positions), positions, GL_STATIC_DRAW);

  glEnableVertexAttribArray(handles.position);
  glVertexAttribPointer(handles.position, 2, GL_FLOAT, GL_FALSE, 0, 0);

  glBindVertexArray(0);

  glClearColor(0.5f, 0.4f, 0.8f, 1.0f);
  CHECK_GL_ERROR();
  return true;
}

int main(int argc, char* argv[]) {
  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); // we can omit the depth buffer in this example
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow(WIN_TITLE);

  glutReshapeFunc(onReshape);
  glutDisplayFunc(onDisplay);
  glutMouseFunc(onMouse);
  glutMotionFunc(onMouseMotion);

  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  if(!init())
    pgr::dieWithError("init failed, cannot continue");

  std::cout << "click and grag the mouse within the window .." << std::endl;
  glutMainLoop();
  return 0;
}
