/**
 * \file texture-skybox-cubemap.cpp
 * \brief Draw "skybox" rendering 2 triangles on the far plane. We use inverse PV matrix to create world direction (cubemap coordinates) from scren space coordinates (NDC).
 * \author Tomas Barak & Jaroslav Sloup
 */

#include <cmath>
#include <iostream>
#include <string>
#include <stdio.h>

#include "trackball.h"
#include "pgr.h"

#if _MSC_VER
#define snprintf _snprintf
#endif

const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 500;
const char * WIN_TITLE = "Skybox done with cube map and far plane rendering";
const unsigned int REFRESH_INTERVAL = 20;
double elapsedTime = 0.0;
glm::mat4 projection = glm::mat4(1.0f);
glm::mat4 model = glm::mat4(1.0f);
glm::mat4 view = glm::mat4(1.0f);
glm::mat4 trackballRotation = glm::mat4(1.0f); // trackball rotation matrix

GLuint cubeShaderProgram = 0;
GLuint cubeVbo = 0;
GLuint cubeTrianglesEao = 0;
GLuint cubeTrianglesVao = 0;

GLuint farplaneVao = 0;
GLuint farplaneVbo = 0;
GLuint farplaneShaderProgram = 0;

GLuint texID = 0;

pgr::CQuaternionTrackball trackball; // trackball class -> uses quaterninons to rotate the scene
int startGrabX, startGrabY; // trackball starting point
int endGrabX, endGrabY; // trackball end point
int winWidth, winHeight; // window width & height

// SELECT MODEL
const pgr::MeshData & meshData = pgr::teapotData;
//const pgr::MeshData & meshData = pgr::monkeyData;
//const pgr::MeshData & meshData = pgr::cubeData;

// SELECT TEXTURE
const char * CUBE_TEXTURE_FILE_PREFIX = "textures/sh";
//const char * CUBE_TEXTURE_FILE_PREFIX = "textures/cm";  //  textures from nVidia demo

// just some simple shader for our scene object
std::string cubeVertexShaderSrc =
  "#version 140\n"
  "uniform mat4 matrix;\n"
  "in vec3 vertex;\n"
  "in vec3 normal;\n"
  "out vec3 color_v;\n"
  "void main()\n"
  "{\n"
  "  color_v = mix(vec3(max(dot(normal, vec3(0.0, 1.0, 0.0)), 0.0)), vec3(0.2), 0.5);\n" // simulate some lighting, using simple diffuse + ambient
  "  gl_Position = matrix * vec4(vertex, 1.0f);\n"
  "}\n"
  ;

// just some simple shader for our scene object
std::string cubeFragmentShaderSrc =
  "#version 140\n"
  "in vec3 color_v;\n"
  "out vec3 color_f;\n"
  "void main()\n"
  "{\n"
  "  color_f = color_v;\n"
  "}\n"
  ;

// each vertex shader receives screen space coordinates and calculates world direction
std::string farplaneVtxShaderSrc =
  "#version 140\n"
  "uniform mat4 invPV;\n"
  "in vec2 screenCoord;\n"
  "out vec3 texCoord_v;\n"
  "void main() {\n"
  "  vec4 farplaneCoord = vec4(screenCoord, 0.9999, 1.0);\n"
  "  vec4 worldViewCoord = invPV * farplaneCoord;\n"
  "  texCoord_v = worldViewCoord.xyz / worldViewCoord.w;\n"
  "  gl_Position = farplaneCoord;\n"
  "}\n"
  ;

// fragment shader uses interpolated 3D tex coords to sample cube map
std::string farplaneFragShaderSrc =
  "#version 140\n"
  "uniform samplerCube skybox;\n"
  "in vec3 texCoord_v;\n"
  "out vec4 color_f;\n"
  "void main() {\n"
  "  color_f = texture(skybox, texCoord_v);\n"
  "}\n"
  ;

bool loadCubeMap( const char * baseFileName ) {
  glActiveTexture(GL_TEXTURE0);

  glGenTextures(1, &texID);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texID);

  const char * suffixes[] = { "posx", "negx", "posy", "negy", "posz", "negz" };
  GLuint targets[] = {
    GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
  };

  for( int i = 0; i < 6; i++ ) {
    std::string texName = std::string(baseFileName) + "_" + suffixes[i] + ".jpg";
    std::cout << "Loading: " << texName << std::endl;
    if(!pgr::loadTexImage2D(texName, targets[i])) {
      std::cerr << __FUNCTION__ << " cannot load image " << texName << std::endl;
      return false;
    }
  }

  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

  // unbind the texture (just in case someone will mess up with texture calls later)
  glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
  CHECK_GL_ERROR();
  return true;
}


bool init() {
  // opengl setup
  glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glPointSize(5.0f);

  // load shaders
  GLuint cubeShaders [] = {
    pgr::createShaderFromSource(GL_VERTEX_SHADER, cubeVertexShaderSrc),
    pgr::createShaderFromSource(GL_FRAGMENT_SHADER, cubeFragmentShaderSrc),
    0
  };
  cubeShaderProgram = pgr::createProgram(cubeShaders);

  // handles to vertex shader inputs
  GLint cubeVertexLoc = glGetAttribLocation(cubeShaderProgram, "vertex");
  GLint cubeNormalLoc = glGetAttribLocation(cubeShaderProgram, "normal");

  GLuint farplaneShaders [] = {
    pgr::createShaderFromSource(GL_VERTEX_SHADER, farplaneVtxShaderSrc),
    pgr::createShaderFromSource(GL_FRAGMENT_SHADER, farplaneFragShaderSrc),
    0
  };
  farplaneShaderProgram = pgr::createProgram(farplaneShaders);

  // buffer for vertices
  glGenBuffers(1, &cubeVbo);
  glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * meshData.nVertices * meshData.nAttribsPerVertex, meshData.verticesInterleaved, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // buffer for triangle indices
  glGenBuffers(1, &cubeTrianglesEao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeTrianglesEao);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * meshData.nTriangles * 3, meshData.triangles, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  // indexed triangles
  glGenVertexArrays(1, &cubeTrianglesVao);
  glBindVertexArray(cubeTrianglesVao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeTrianglesEao);
  glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);
  glEnableVertexAttribArray(cubeVertexLoc);
  glEnableVertexAttribArray(cubeNormalLoc);
  glVertexAttribPointer(cubeVertexLoc, 3, GL_FLOAT, GL_FALSE, meshData.nAttribsPerVertex * sizeof(float), (void *)(0));
  glVertexAttribPointer(cubeNormalLoc, 3, GL_FLOAT, GL_FALSE, meshData.nAttribsPerVertex * sizeof(float), (void *)(3 * sizeof(float)));

  // 2D coordinates of 2 triangles covering the whole screen (NDC), draw using strip
  static const float screenCoords[] = {
    -1.0f, -1.0f,
    1.0f, -1.0f,
    -1.0f, 1.0f,
    1.0f, 1.0f
  };
  // buffer for far plane rendering
  glGenBuffers(1, &farplaneVbo);
  glBindBuffer(GL_ARRAY_BUFFER, farplaneVbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(screenCoords), screenCoords, GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // handles to vertex shader inputs
  GLint farplaneSceenCoordLoc = glGetAttribLocation(farplaneShaderProgram, "screenCoord");
  GLint skyboxLoc = glGetUniformLocation(farplaneShaderProgram, "skybox");
  glUseProgram(farplaneShaderProgram);
  glUniform1i(skyboxLoc, 0);

  glGenVertexArrays(1, &farplaneVao);
  glBindVertexArray(farplaneVao);
  glBindBuffer(GL_ARRAY_BUFFER, farplaneVbo);
  glEnableVertexAttribArray(farplaneSceenCoordLoc);
  glVertexAttribPointer(farplaneSceenCoordLoc, 2, GL_FLOAT, GL_FALSE, 0, 0);

  glBindVertexArray(0);
  glUseProgram(0);
  CHECK_GL_ERROR();

  if(!loadCubeMap(CUBE_TEXTURE_FILE_PREFIX)) {
    std::cerr << "cannot continue without cubemap" << std::endl;
    return false;
  }

  // initialize trackball -> drag from point [0,0] to point [0,0]
  trackball.setRotation(0.0f, 0.0f, 0.0f, 0.0f);
  return true;
}

// deletes allocated buffers
void cleanup() {
  glDeleteVertexArrays(1, &cubeTrianglesVao);
  glDeleteBuffers(1, &cubeTrianglesEao);
  glDeleteBuffers(1, &cubeVbo);
  glDeleteVertexArrays(1, &farplaneVao);
  glDeleteBuffers(1, &farplaneVbo);
  pgr::deleteProgramAndShaders(cubeShaderProgram);
  pgr::deleteProgramAndShaders(farplaneShaderProgram);
}

void refreshCb(int) {
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);

  model = glm::mat4(1.0f);
  view = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -5.0f)) * trackballRotation;

  glutPostRedisplay();
}

void displayCb() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // compose model transformations
  glm::mat4 matrix = projection * view * model;

  glUseProgram(cubeShaderProgram);
  glUniformMatrix4fv(glGetUniformLocation(cubeShaderProgram, "matrix"), 1, GL_FALSE, glm::value_ptr(matrix));

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texID);

  glBindVertexArray(cubeTrianglesVao);
  glDrawElements(GL_TRIANGLES, meshData.nTriangles * 3, GL_UNSIGNED_INT, 0);

  // crate view rotation matrix by using view matrix with cleared translation
  glm::mat4 viewRotation = view;
  viewRotation[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
  // vertex shader will translate screen space coordinates (NDC) using inverse PV matrix
  glm::mat4 invPV = glm::inverse(projection * viewRotation);
  // draw "skybox" rendering 2 triangles covering far plane
  glBindVertexArray(farplaneVao);
  glUseProgram(farplaneShaderProgram);
  GLint invPVLoc = glGetUniformLocation(farplaneShaderProgram, "invPV");
  glUniformMatrix4fv(invPVLoc, 1, GL_FALSE, glm::value_ptr(invPV));
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  glutSwapBuffers();
}

void reshapeCb(int w, int h) {
  winWidth = w;
  winHeight = h;

  glViewport(0, 0, w, h);
  projection =  glm::perspective(60.0f, float(w) / float(h) , 1.0f, 10.0f);
}

void keyboardCb(unsigned char key, int x, int y) {
  switch (key) {
    case 27:
      glutLeaveMainLoop();
      break;
  }
}

void passiveMouseCb(int x, int y) {
  endGrabX = x;
  endGrabY = y;

  if(startGrabX != endGrabX || startGrabY != endGrabY) {

    /* get rotation from trackball using quaternion */
    trackball.addRotation(startGrabX, startGrabY, endGrabX, endGrabY, winWidth, winHeight);

    /* build rotation matrix from quaternion */
    trackball.getRotationMatrix(trackballRotation);

    startGrabX = endGrabX;
    startGrabY = endGrabY;
  }
}

void mouseCb(int button, int state, int x, int y) {
  if(button == GLUT_LEFT_BUTTON) {
    if(state == GLUT_DOWN) {
      startGrabX = x;
      startGrabY = y;
      glutMotionFunc(passiveMouseCb);
      return;
    } else {
      glutMotionFunc(NULL);
    }
  }
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE | GLUT_DEBUG);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow(WIN_TITLE);

  glutDisplayFunc(displayCb);
  glutReshapeFunc(reshapeCb);
  glutKeyboardFunc(keyboardCb);
  glutMouseFunc(mouseCb);
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);

  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  if(!init())
    pgr::dieWithError("cannot proceed");

  std::cout << "Skybox using farplane rendering. Drag mouse to orbit, press Esc to quit." << std::endl;

  glutMainLoop();
  cleanup();
  return 0;
}
