#version 140

uniform sampler2D diffuseTex;
uniform sampler2D specularTex;

#define NLIGHTS 3
struct Light {
  vec3 position;
  vec3 color;
};
uniform Light lights[NLIGHTS];
in vec3 normal_v;
in vec3 position_v;
in vec2 texCoords_v;
out vec3 color_f;

void main() {
  vec3 Ka = vec3(0.0);
  vec3 Kd = 0.5 * texture(diffuseTex, texCoords_v).xyz;
  vec3 Ks = 2.0 * texture(specularTex, texCoords_v).xyz;
  vec3 color = vec3(0.0);
  vec3 N = normalize(normal_v);
  for(int l = 0; l < NLIGHTS; ++l) {
    vec3 L = normalize(lights[l].position - position_v);
    vec3 C = normalize(-position_v);
    vec3 R = reflect(-L, N);
    float dist = length(lights[l].position - position_v);
    float attenuation = min(1.0 / (dist * dist), 1.0);

    color += Ka * lights[l].color;
    color += Kd * lights[l].color * max(dot(N, L), 0.0) * attenuation;
    color += Ks * lights[l].color * pow(max(dot(R, C), 0.0), 42.0) * attenuation;
  }

  color_f = color;
}

