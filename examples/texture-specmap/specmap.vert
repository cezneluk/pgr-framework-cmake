#version 140

uniform mat4 VM;
uniform mat4 PVM;

in vec3 vertex;
in vec3 normal;
in vec2 texCoords;
out vec3 normal_v;
out vec3 position_v;
out vec2 texCoords_v;

void main() {
  vec4 nor4 = VM * vec4(normal, 0.0);
  normal_v = nor4.xyz;
  vec4 pos4 = VM * vec4(vertex, 1.0);
  position_v = pos4.xyz / pos4.w;
  gl_Position = PVM * vec4(vertex, 1.0);
  texCoords_v = texCoords;
}
