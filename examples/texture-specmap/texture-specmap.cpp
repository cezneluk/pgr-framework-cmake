/**
 * \file texture-specmap.cpp
 * \brief Displays a cube with diffuse and specular map.
 * \author Tomas Barak & Jaroslav Sloup
 */

#include <cmath>
#include <iostream>
#include <string>
#include <stdio.h>

#include "pgr.h"
#include "AntTweakBar.h"

#if _MSC_VER
#define snprintf _snprintf
#endif

const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 500;
const char * WIN_TITLE = "Multiple textures - diffuse, specular maps - demo";
const unsigned int REFRESH_INTERVAL = 33;
const int NLIGHTS = 3;
const char *fShaders[2] = {
  "specmap.frag", "modulate.frag"
};
int currentShader = 0; // specmap

glm::mat4 projection = glm::mat4(1.0f);
glm::mat4 view = glm::mat4(1.0f);
glm::mat4 model = glm::mat4(1.0f);

GLuint cubeShaderProgram = 0;
GLuint cubeVbo = 0;
GLuint cubeTrianglesEao = 0;
GLuint cubeTrianglesVao = 0;

GLuint pointsVao = 0;
GLuint pointsVbo = 0;
GLuint pointsShaderProgram = 0;

GLuint diffuseTex = 0;
GLuint specularTex = 0;

glm::quat viewRotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
glm::quat modelRotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
TwBar * bar = 0;

//const pgr::MeshData & meshData = pgr::monkeyData;
const pgr::MeshData & meshData = pgr::cubeData;

struct Light {
  Light(float r = 1.0f, float g = 1.0f, float b = 1.0f):
    color(r, g, b, 0.0f), position(0.0f, 0.0f, 0.0f, 1.0f) {}

  glm::vec4 position;
  glm::vec4 color;
};

Light lights[NLIGHTS] = {
  Light(1.2f, 1.2f, 1.3f),
  Light(1.2f, 1.3f, 0.0f),
  Light(0.0f, 1.1f, 1.3f)
};

// vertex shader just sends the color to the FS and translates the position
std::string pointsVertexShaderSrc =
    "#version 140\n"
    "uniform mat4 PV;\n"
    "in vec4 position;\n"
    "in vec4 color;\n"
    "out vec4 color_v;\n"
    "void main()\n"
    "{\n"
    "  gl_Position = PV * position;\n"
    "  color_v = color;\n"
    "}\n"
    ;

// only writes interpolated color
std::string pointsFragmentShaderSrc =
    "#version 140\n"
    "in vec4 color_v;\n"
    "out vec3 color_f;\n"
    "void main()\n"
    "{\n"
    "  color_f = color_v.rgb;\n"
    "}\n"
    ;

bool reloadCubeShader(const char * fs) {
  if(cubeShaderProgram != 0)
    pgr::deleteProgramAndShaders(cubeShaderProgram);
  // load shaders
  GLuint cubeShaders [] = {
    pgr::createShaderFromFile(GL_VERTEX_SHADER, "specmap.vert"),
    pgr::createShaderFromFile(GL_FRAGMENT_SHADER, fs),
    0
  };
  cubeShaderProgram = pgr::createProgram(cubeShaders);
  if(cubeShaderProgram == 0)
    return false;

  // handles to vertex shader inputs
  GLint cubeVertexLoc = glGetAttribLocation(cubeShaderProgram, "vertex");
  GLint cubeNormalLoc = glGetAttribLocation(cubeShaderProgram, "normal");
  GLint cubeTexCoordsLoc = glGetAttribLocation(cubeShaderProgram, "texCoords");

  // indexed triangles
  glBindVertexArray(cubeTrianglesVao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeTrianglesEao);
  glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);

  glEnableVertexAttribArray(cubeVertexLoc);
  glEnableVertexAttribArray(cubeNormalLoc);
  glEnableVertexAttribArray(cubeTexCoordsLoc);

  glVertexAttribPointer(cubeVertexLoc, 3, GL_FLOAT, GL_FALSE, meshData.nAttribsPerVertex * sizeof(float), (void *)(0));
  glVertexAttribPointer(cubeNormalLoc, 3, GL_FLOAT, GL_FALSE, meshData.nAttribsPerVertex * sizeof(float), (void *)(3*sizeof(float)));
  glVertexAttribPointer(cubeTexCoordsLoc, 2, GL_FLOAT, GL_FALSE, meshData.nAttribsPerVertex * sizeof(float), (void *)(6*sizeof(float)));

  glBindVertexArray(0);

  GLint diffuseTexLoc = glGetUniformLocation(cubeShaderProgram, "diffuseTex");
  GLint specularTexLoc= glGetUniformLocation(cubeShaderProgram, "specularTex");

  glUseProgram(cubeShaderProgram);
  glUniform1i(diffuseTexLoc, 0);
  glUniform1i(specularTexLoc, 1);
  glUseProgram(0);
  return true;
}

void TW_CALL changeShaderCb(const void *value, void *) {
  currentShader = *(int*)value;
  reloadCubeShader(fShaders[currentShader]);
  std::cout << "using: " << fShaders[currentShader] << std::endl;
}

void TW_CALL getShaderCb(void *value, void *clientData) {
  *(int*)value = currentShader;
}

void initGui() {
  bar = TwNewBar("TweakBar");
  TwDefine(" TweakBar size='200 300' color='96 216 224' ");

  TwEnumVal shaderEV[2] = {{0, "Specular Map"}, {1, "Modulate"}};
  TwType shaderType = TwDefineEnum("ShaderEnum", shaderEV, 2);
  TwAddVarCB(bar, "shader", shaderType, changeShaderCb, getShaderCb, 0, " label='Shader' help='Change shader.' ");
  TwAddVarRW(bar, "viewRotation", TW_TYPE_QUAT4F, &viewRotation, " label='View rotation' opened=true ");
  TwAddVarRW(bar, "modelRotation", TW_TYPE_QUAT4F, &modelRotation, " label='Model rotation' opened=true ");
}

void init() {
  // opengl setup
  glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glPointSize(5.0f);

  // buffer for vertices
  glGenBuffers(1, &cubeVbo);
  glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * meshData.nVertices * meshData.nAttribsPerVertex, meshData.verticesInterleaved, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // buffer for triangle indices
  glGenBuffers(1, &cubeTrianglesEao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeTrianglesEao);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * meshData.nTriangles * 3, meshData.triangles, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  glGenVertexArrays(1, &cubeTrianglesVao);
  if(!reloadCubeShader(fShaders[currentShader]))
    pgr::dieWithError("cannot load shader");

  GLuint pointsShaders [] = {
    pgr::createShaderFromSource(GL_VERTEX_SHADER, pointsVertexShaderSrc),
    pgr::createShaderFromSource(GL_FRAGMENT_SHADER, pointsFragmentShaderSrc),
    0
  };
  pointsShaderProgram = pgr::createProgram(pointsShaders);

  // handles to vertex shader inputs
  GLint pointsVertexLoc = glGetAttribLocation(pointsShaderProgram, "position");
  GLint pointsColorLoc = glGetAttribLocation(pointsShaderProgram, "color");

  // buffer for light "dots" rendering
  glGenBuffers(1, &pointsVbo);
  glBindBuffer(GL_ARRAY_BUFFER, pointsVbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(lights), &lights, GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glGenVertexArrays(1, &pointsVao);
  glBindVertexArray(pointsVao);
  glBindBuffer(GL_ARRAY_BUFFER, pointsVbo);
  glEnableVertexAttribArray(pointsVertexLoc);
  glEnableVertexAttribArray(pointsColorLoc);
  glVertexAttribPointer(pointsVertexLoc, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(0));
  glVertexAttribPointer(pointsColorLoc, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(4 * sizeof(float)));

  glBindVertexArray(0);
  CHECK_GL_ERROR();

  // load textures
  diffuseTex = pgr::createTexture("textures/brick01-diff.png");
  if(diffuseTex == 0)
    pgr::dieWithError("Texture loading failed.");

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, diffuseTex);

  specularTex = pgr::createTexture("textures/brick01-spec.png");
  if(specularTex == 0)
    pgr::dieWithError("Texture loading failed.");

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, specularTex);

  CHECK_GL_ERROR();
  initGui();
}

// deletes allocated buffers
void cleanup() {
  glDeleteVertexArrays(1, &cubeTrianglesVao);
  glDeleteBuffers(1, &cubeTrianglesEao);
  glDeleteBuffers(1, &cubeVbo);
  glDeleteVertexArrays(1, &pointsVao);
  glDeleteBuffers(1, &pointsVbo);
  pgr::deleteProgramAndShaders(cubeShaderProgram);
  pgr::deleteProgramAndShaders(pointsShaderProgram);
}

void refreshCb(int) {
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);

  float cubeCenterOff = -4.0f;
  float radius = 2.5f;
  float l0rotAngleDeg = 60.0f;
  float l1rotAngleDeg = 40.0f;

  lights[0].position = glm::vec4(radius * cos(glm::radians(l0rotAngleDeg)), radius * sin(glm::radians(l0rotAngleDeg)), 0.0f, 1.0f);
  lights[1].position = glm::vec4(radius * cos(glm::radians(l1rotAngleDeg)), 0.0f, radius * sin(glm::radians(l1rotAngleDeg)), 1.0f);
  lights[2].position = glm::vec4(0.0f, radius * cos(glm::radians(l1rotAngleDeg)), radius * sin(glm::radians(l1rotAngleDeg)), 1.0f);

  model = glm::mat4_cast(modelRotation);
  view = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, cubeCenterOff)) * glm::mat4_cast(viewRotation);

  glutPostRedisplay();
}

void displayCb() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glm::mat4 VM = view * model;
  glm::mat4 PVM = projection * view * model;
  glm::mat4 PV = projection * view;

  glUseProgram(cubeShaderProgram);
  glUniformMatrix4fv(glGetUniformLocation(cubeShaderProgram, "VM"), 1, GL_FALSE, glm::value_ptr(VM));
  glUniformMatrix4fv(glGetUniformLocation(cubeShaderProgram, "PVM"), 1, GL_FALSE, glm::value_ptr(PVM));

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, diffuseTex);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, specularTex);

  for(int l = 0; l < NLIGHTS; ++l) {
    char buf[255];
    snprintf(buf, 255, "lights[%i].position", l);
    glUniform3fv(glGetUniformLocation(cubeShaderProgram, buf), 1, glm::value_ptr(view * lights[l].position));
    snprintf(buf, 255, "lights[%i].color", l);
    glUniform3fv(glGetUniformLocation(cubeShaderProgram, buf), 1, glm::value_ptr(lights[l].color));
  }

  glBindVertexArray(cubeTrianglesVao);
  glDrawElements(GL_TRIANGLES, meshData.nTriangles * 3, GL_UNSIGNED_INT, 0);

  glUseProgram(pointsShaderProgram);
  glUniformMatrix4fv(glGetUniformLocation(pointsShaderProgram, "PV"), 1, GL_FALSE, glm::value_ptr(PV));
  glBindVertexArray(pointsVao);
  glBindBuffer(GL_ARRAY_BUFFER, pointsVbo);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(lights), &lights);
  glDrawArrays(GL_POINTS, 0, NLIGHTS);

  TwDraw();
  glutSwapBuffers();
}

void reshapeCb(int w, int h) {
  glViewport(0, 0, w, h);
  projection =  glm::perspective(60.0f, float(w) / float(h) , 1.0f, 10.0f);
  TwWindowSize(w, h);
}

void keyboardCb(unsigned char key, int x, int y) {
  TwEventKeyboardGLUT(key, x, y);
  switch (key) {
    case 27:
      glutLeaveMainLoop();
      break;
  }
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE | GLUT_DEBUG);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow(WIN_TITLE);

  TwInit(TW_OPENGL_CORE, NULL);

  glutDisplayFunc(displayCb);
  glutReshapeFunc(reshapeCb);
  glutKeyboardFunc(keyboardCb);

  glutMouseFunc((GLUTmousebuttonfun)TwEventMouseButtonGLUT);
  glutMotionFunc((GLUTmousemotionfun)TwEventMouseMotionGLUT);
  glutPassiveMotionFunc((GLUTmousemotionfun)TwEventMouseMotionGLUT);
  //glutKeyboardFunc((GLUTkeyboardfun)TwEventKeyboardGLUT);
  glutSpecialFunc((GLUTspecialfun)TwEventSpecialGLUT);

  TwGLUTModifiersFunc(glutGetModifiers);

  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  init();

  std::cout << "Three lights orbiting a cube. Press Esc to quit." << std::endl;
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);

  glutMainLoop();

  cleanup();
  TwTerminate();
  return 0;
}
