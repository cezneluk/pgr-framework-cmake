#version 140
in vec4 color_v;
out vec4 color_f;
uniform sampler2D spriteTex;
void main() {
  float alpha = texture(spriteTex, gl_PointCoord).x;
  color_f = vec4(color_v.xyz, color_v.w * alpha);
}
