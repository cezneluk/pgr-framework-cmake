#version 140
uniform mat4 matrix;
uniform sampler1D ageSizeTex;
uniform sampler1D ageColorTex;
in vec3 vertex;
in float age;
out vec4 color_v;
void main() {
  color_v = texture(ageColorTex, age);
  gl_PointSize = texture(ageSizeTex, age).x;
  gl_Position = matrix * vec4(vertex, 1.0f);
}
