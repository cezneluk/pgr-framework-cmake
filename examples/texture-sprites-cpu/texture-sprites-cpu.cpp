/**
 * \file texture-sprites-cpu.cpp
 * \brief Simple particle demo using point sprites. Simulation is done on the CPU.
 * \author Tomas Barak
 */

#include <iostream>
#include <stdlib.h>

#include "pgr.h"

#if _MSC_VER
#define snprintf _snprintf
#endif

const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 500;
const char * WIN_TITLE = "Particles using point sprites demo";
const unsigned int REFRESH_INTERVAL = 20;
double elapsedTime = 0.0;

glm::mat4 projection = glm::mat4(1.0f);
glm::mat4 view = glm::mat4(1.0f);
glm::mat4 model = glm::mat4(1.0f);

GLuint particlesShader = 0;
GLuint particlesVao = 0;
GLuint particlesVbo = 0;
GLint matrixLoc = -1;
GLuint spriteTex = 0;
GLuint ageColorTex = 0;
GLuint ageSizeTex = 0;

struct Particle {
  glm::vec3 position;
  float age;
  glm::vec3 speed;
};

Particle * particles = 0;
int nParticles = 5000;
glm::vec3 particleGlobalForce = glm::vec3(0.0f, 8.0f, 0.0f);
glm::vec3 particleSourcePos = glm::vec3(-3.0f, -2.0f, 0.0f);
glm::vec3 particleSourceSpeed = glm::vec3(4.0f, -0.4f, 0.0f);
float maxAge = 1.0f;
const int ageColorDataSize = 4;
float ageColorData[ageColorDataSize * 4] = {
  1.0f, 0.9f, 0.9f, 0.1f,
  1.0f, 0.5f, 0.0f, 0.3f,
  0.9f, 0.3f, 0.1f, 0.2f,
  0.2f, 0.2f, 0.2f, 0.01f,
};
const int ageSizeDataSize = 2;
float ageSizeData[ageSizeDataSize] = {
  22.0f,
  64.0f,
};

glm::vec3 randomVec3() {
  return glm::vec3(rand(), rand(), rand()) / float(RAND_MAX);
}

void restartParticle(Particle * particle) {
  particle->position = particleSourcePos;
  particle->speed = particleSourceSpeed * 0.6f + particleSourceSpeed * randomVec3() * 0.4f;
  particle->age = float(rand()) / float(RAND_MAX) * 0.2f;
  particle->position += particle->speed * float(rand()) / float(RAND_MAX) * 0.2f;
}

void simulateParticles(float timeDiff) {
  for(int i = 0; i < nParticles; ++i) {
    particles[i].position += particles[i].speed * timeDiff;
    particles[i].speed += particleGlobalForce * timeDiff;
    particles[i].age += timeDiff / maxAge;

    if(particles[i].age > 1.0f)
      restartParticle(particles + i);
  }
  glBindBuffer(GL_ARRAY_BUFFER, particlesVbo);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Particle) * nParticles, particles);
}

void init() {
  srand(0); // initialize random number generator
  particles = new Particle[nParticles];
  for(int i = 0; i < nParticles; ++i) {
    restartParticle(particles + i);
    particles[i].age = float(i) / float(nParticles);
  }
  elapsedTime = 0.001 * (double)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

  // opengl setup
  glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_PROGRAM_POINT_SIZE);

  // load shaders
  GLuint shaders [] = {
    pgr::createShaderFromFile(GL_VERTEX_SHADER, "particles.vert"),
    pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "particles.frag"),
    0
  };
  particlesShader = pgr::createProgram(shaders);
  if(particlesShader == 0)
    pgr::dieWithError("Shader loading failed.");

  // handles to vertex shader inputs
  GLint vertexLoc = glGetAttribLocation(particlesShader, "vertex");
  GLint ageLoc = glGetAttribLocation(particlesShader, "age");
  matrixLoc = glGetUniformLocation(particlesShader, "matrix");
  GLint spriteTexLoc = glGetUniformLocation(particlesShader, "spriteTex");
  GLint ageColorTexLoc = glGetUniformLocation(particlesShader, "ageColorTex");
  GLint ageSizeTexLoc = glGetUniformLocation(particlesShader, "ageSizeTex");
  glUseProgram(particlesShader);
  glUniform1i(spriteTexLoc, 0);  // sprite alpha texture in the first texture unit
  glUniform1i(ageColorTexLoc, 1); // particle color in the second unit
  glUniform1i(ageSizeTexLoc, 2); // particle size in the third unit
  glUseProgram(0);

  // VAO + buffer for particles
  glGenVertexArrays(1, &particlesVao);
  glBindVertexArray(particlesVao);
  glGenBuffers(1, &particlesVbo);
  glBindBuffer(GL_ARRAY_BUFFER, particlesVbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Particle) * nParticles, particles, GL_DYNAMIC_DRAW);

  glEnableVertexAttribArray(vertexLoc);
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), 0);
  glEnableVertexAttribArray(ageLoc);
  glVertexAttribPointer(ageLoc, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)sizeof(glm::vec3));

  // load sprite alpha texture
  spriteTex = pgr::createTexture("particle.png");
  if(spriteTex == 0)
    pgr::dieWithError("Texture loading failed.");

  // generate particle color and size textures
  glGenTextures(1, &ageColorTex);
  glBindTexture(GL_TEXTURE_1D, ageColorTex);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, ageColorDataSize, 0, GL_RGBA, GL_FLOAT, ageColorData);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glGenTextures(1, &ageSizeTex);
  glBindTexture(GL_TEXTURE_1D, ageSizeTex);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, ageSizeDataSize, 0, GL_RED, GL_FLOAT, ageSizeData);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
}

// deletes allocated buffers
void cleanup() {
  glDeleteVertexArrays(1, &particlesVao);
  glDeleteBuffers(1, &particlesVbo);
  glDeleteTextures(1, &spriteTex);
  glDeleteTextures(1, &ageColorTex);
  glDeleteTextures(1, &ageSizeTex);
  pgr::deleteProgramAndShaders(particlesShader);
  if(particles)
    delete [] particles;
}

void refreshCb(int) {
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);
  double currentTime = 0.001 * (double)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds
  double timeDiff = currentTime - elapsedTime;
  elapsedTime = currentTime;
  simulateParticles(timeDiff);

  model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -5));
  view = glm::mat4(1.0f);

  glutPostRedisplay();
}

void displayCb() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glm::mat4 matrix = projection * view * model;

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, spriteTex);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_1D, ageColorTex);
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_1D, ageSizeTex);

  glUseProgram(particlesShader);
  glUniformMatrix4fv(matrixLoc, 1, GL_FALSE, glm::value_ptr(matrix));

  glDepthMask(GL_FALSE);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBindVertexArray(particlesVao);
  glDrawArrays(GL_POINTS, 0, nParticles);
  glDisable(GL_BLEND);
  glDepthMask(GL_TRUE);

  glutSwapBuffers();
}

void reshapeCb(int w, int h) {
  glViewport(0, 0, w, h);
  projection =  glm::perspective(60.0f, float(w) / float(h) , 1.0f, 10.0f);
}

void keyboardCb(unsigned char key, int x, int y) {
  switch (key) {
    case 27:
      glutLeaveMainLoop();
      break;
  }
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);

  glutInitContextVersion(3, 2);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE | GLUT_DEBUG);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow(WIN_TITLE);

  glutDisplayFunc(displayCb);
  glutReshapeFunc(reshapeCb);
  glutKeyboardFunc(keyboardCb);
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);

  if(!pgr::initialize(3, 2))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  init();

  std::cout << "Particle system. Press Esc to quit." << std::endl;

  glutMainLoop();
  cleanup();
  return 0;
}
