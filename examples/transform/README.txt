Demonstrace transformac�

   SPACE - p�ep�n� mezi kostkami
   s,S   - p�ep�n� vzta�nou soustavu, v��i n� se rotuje

Odkomentov�n� 15. ��dku p�ipoj� lad�c� koment��e v k�du dle extenze GL_GREMEDY_string_marker
//#define GDEBUGGER // debugging comments by Graphics Remedy

Kostky v z�kladn� poloze, tj. bez rotace
  PVMmatrix = projection * view * cubes[i].model;
  
    
Rotace kostek:

Ka�d� kostka podle sv� osy:
  PVMmatrix = projection * view * cubes[i].model * R;

Rotace v�ech kostek okolo osy ve sv�tov� soustav� - world space 
  PVMmatrix = projection * view * R* cubes[i].model; 

Rotace sc�ny ve soustav� kamery - camera space
  PVMmatrix = projection * R * view * cubes[i].model; 

Rotace pohledov�ho jehlanu v o�ezov�ch sou�adnic�ch - nesmysl, ale je tu 
jako dal�� mo�n� pozice R v �et�zci transformac�
  PVMmatrix = R * projection * view * cubes[i].model; 


Rotace cel� sc�ny okolo vybran� kostky (s matic� A) - podle osy vybran� kostky
  PVMmatrix = projection * view * A * R * Ainv * cubes[i].model;
  
 kde  
  glm::mat4 A = cubes[selectedCubeIdx].model;
  glm::mat4 Ainv = glm::inverse(A);
  
  
Rotace kostek, ka�d� kolem sv�ho st�edu, ale podle osy kamery
POZOR - v matici view je inverzn� matice E^{-1} a my pot�ebujeme matici E. 
        Proto   E = glm::inverse(view) !!!!!!!!

  glm::mat4 B = modelTranslation * viewRotation;
      glm::mat4 Binv = glm::inverse(B);
      PVMmatrix = projection * view *B * R * Binv *  cubes[i].model;  
  

Rotace kostek kolem st�edu vybran� kostky, ale podle osy kamery
  
  glm::mat4 B = modelTranslation * viewRotation;
      glm::mat4 Binv = glm::inverse(B);
      PVMmatrix = projection * view *B * R * Binv *  cubes[selectedCubeIdx].model;  
 
Ostatn� transformace v k�du transform.cpp


Rotace bodu kolem objektov� osy (bod krou�� okolo st�edu vybran� kostky)
  PVMmatrix = projection * view * cubes[selectedCubeIdx].model * R * T;