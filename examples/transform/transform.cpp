/**
 * \file transform.cpp 
 * \brief Simple composition of affine transformation
 * \author Petr Felkel, based on primitives-cube by Tomas Barak
 */

#include <cmath>
#include <iostream>
#include <stdio.h>  //_snprintf
#if _MSC_VER
#define snprintf _snprintf_s
#endif

#include "pgr.h"
//#define GDEBUGGER // debugging comments by Graphics Remedy
#ifdef GDEBUGGER
#include "GRemedyGLExtensions.h"
#endif 

const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 500;
const char * WIN_TITLE = "PGR - Composition of Affine Transformations";
const unsigned int REFRESH_INTERVAL = 33;
const float NEAR_PLANE = 1.0f;
const float FAR_PLANE  = 25.0f;



double elapsedTime = 0.0;

glm::mat4 view        = glm::lookAt(glm::vec3(3.0f, 3.0f, 10.0f), glm::vec3(0.0f, 0.0f, -2.0f), glm::vec3(0.5f, 1.0f, 0.0f) );
glm::mat4 projection  = glm::mat4(1.0f);
float rotAngleDeg     = 0.0f;

enum {NO_ROTATION, OBJECT_SPACE, WORLD_SPACE, CAMERA_SPACE, PROJECTIVE_SPACE, 
      SELECTED_CUBE_CENTER_AND_AXIS, SELECTED_CUBE_CENTER__CAMERA_AXIS, SELECTED_CUBE_CENTER__WORLD_AXIS, 
      OBJECT_SPACE__CAMERA_AXIS, OBJECT_SPACE__WORLD_AXIS, 
      SPACE_COUNT};  // maximal enum
//unsigned int selectedSpace = OBJECT_SPACE;
unsigned int selectedSpace = NO_ROTATION;

struct Cube
{
  glm::mat4 model;   // model matrix - O

  Cube(const glm::mat4 & model_matrix) : model(model_matrix) {}

  static GLuint shaderProgram;
  static GLuint vao;
  static GLuint eao;
  static GLuint vbo;
};



Cube cubes[] = {            // de facto list of model transformation matrices...
  Cube( glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(0.1f, 0.1f, 0.1f)), glm::vec3(2.5f, -10.0f, 0.0f)) ),             // middle small 
  Cube( glm::translate(glm::rotate(glm::mat4(1.0f),45.0f,  glm::vec3(1.0f, 1.0f,1.0f)), glm::vec3(-0.5f,  0.1f, -6.0f)) ),   // top left
  Cube( glm::translate(glm::rotate(glm::mat4(1.0f),30.0f,  glm::vec3(1.0f, 0.0f,0.0f)), glm::vec3( 0.5f,  0.1f, -5.0f)) ),   // top middle
  Cube( glm::translate(glm::rotate(glm::mat4(1.0f),60.0f,  glm::vec3(0.0f, 0.0f,1.0f)), glm::vec3( 5.5f, -1.1f, -5.0f)) ),   // top right
  Cube( glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(1.0f, 1.0f, 1.0f)), glm::vec3(5.0f, -0.6f, -3.0f)) ),            // right big 
};

GLuint Cube::shaderProgram = 0;
GLuint Cube::vao = 0;
GLuint Cube::eao = 0;
GLuint Cube::vbo = 0;
GLuint selectedCubeIdx = 0;



// interleaved array of cube vertices
const unsigned nCubeVertices = 8 + 6;
const unsigned nCubeAttribsPerVertex = 6; // x,y,z position, r, g, b color
const float cubeVertices[nCubeVertices * nCubeAttribsPerVertex] = {
// x      y      z       r     g     b
  -1.0f, -1.0f, -1.0f,   0.0f, 0.0f, 0.0f, // 0
  -1.0f, -1.0f,  1.0f,   0.0f, 0.0f, 1.0f, // 1
  -1.0f,  1.0f, -1.0f,   0.0f, 1.0f, 0.0f, // 2
  -1.0f,  1.0f,  1.0f,   0.0f, 1.0f, 1.0f, // 3
   1.0f, -1.0f, -1.0f,   1.0f, 0.0f, 0.0f, // 4
   1.0f, -1.0f,  1.0f,   1.0f, 0.0f, 1.0f, // 5
   1.0f,  1.0f, -1.0f,   1.0f, 1.0f, 0.0f, // 6
   1.0f,  1.0f,  1.0f,   1.0f, 1.0f, 1.0f, // 7
   0.0f,  0.0f,  0.0f,   1.0f, 0.0f, 0.0f, // center (X)
   1.5f,  0.0f,  0.0f,   1.0f, 0.0f, 0.0f, // axis X

   0.0f,  0.0f,  0.0f,   0.0f, 1.0f, 0.0f, // center Y
   0.0f,  1.5f,  0.0f,   0.0f, 1.0f, 0.0f, // axis Y

   0.0f,  0.0f,  0.0f,   0.0f, 0.0f, 1.0f, // center Z
   0.0f,  0.0f,  1.5f,   0.0f, 0.0f, 1.0f, // axis Z


};

// indices used to draw cube as GL_LINES
const unsigned nCubeLineIdx = 24;
const unsigned short cubeLineIdx[nCubeLineIdx] = {
  0, 4, // lower face
  4, 5,
  5, 1,
  1, 0,
  2, 6, // upper face
  6, 7,
  7, 3,
  3, 2,
  0, 2, // side lines
  4, 6,
  1, 3,
  5, 7,
};


std::string vertexShaderSrc =
    "#version 140\n"
    "uniform mat4 PVMmatrix;\n"
    "in vec3 vertex;\n"
    "in vec3 color;\n"
    "out vec3 color_v;\n"
    "void main()\n"
    "{\n"
    "  color_v = color;\n"
    "  gl_Position = PVMmatrix * vec4(vertex, 1.0f);\n"
    //"  gl_Position = gl_Position / gl_Position.w;\n"
    "}\n"
    ;

std::string fragmentShaderSrc =
    "#version 140\n"
    "in vec3 color_v;\n"
    "out vec3 color_f;\n"
    "void main()\n"
    "{\n"
    "  color_f = color_v;\n"
    "}\n"
    ;

#ifdef GDEBUGGER
int isExtensionSupported(const char *extension)
{							      
  GLint nExtensions;

  glGetIntegerv( GL_NUM_EXTENSIONS, &nExtensions );

  for( int i=0; i < nExtensions; i++ )
  {
     const char* charptr = (const char*)glGetStringi( GL_EXTENSIONS, i );
     if( 0 == strcmp( extension, 
              (const char*)glGetStringi( GL_EXTENSIONS, i )))
     return 1;   // exact extension name found   
  }
  return 0; 
}
#endif 


void init() {
  // opengl setup
  glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glPointSize(5.0f);

  // load shader
  std::vector<GLuint> shaders;
  shaders.push_back(pgr::createShaderFromSource(GL_VERTEX_SHADER, vertexShaderSrc));
  shaders.push_back(pgr::createShaderFromSource(GL_FRAGMENT_SHADER, fragmentShaderSrc));
  Cube::shaderProgram = pgr::createProgram(shaders);

  // handles to vertex shader inputs
  GLint vertexLoc = glGetAttribLocation(Cube::shaderProgram, "vertex");
  GLint colorLoc  = glGetAttribLocation(Cube::shaderProgram, "color");

  // buffer for vertices
  glGenBuffers(1, &Cube::vbo);
  glBindBuffer(GL_ARRAY_BUFFER, Cube::vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // buffer for line indices
  glGenBuffers(1, &Cube::eao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Cube::eao);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeLineIdx), cubeLineIdx, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  // indexed lines
  glGenVertexArrays(1, &Cube::vao);
  glBindVertexArray(Cube::vao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Cube::eao);
  glBindBuffer(GL_ARRAY_BUFFER, Cube::vbo);
  glEnableVertexAttribArray(vertexLoc);
  glEnableVertexAttribArray(colorLoc);
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, nCubeAttribsPerVertex * sizeof(float), (void *)(0));
  glVertexAttribPointer(colorLoc,  3, GL_FLOAT, GL_FALSE, nCubeAttribsPerVertex * sizeof(float), (void *)(3 * sizeof(float)));

  glBindVertexArray(0);
  CHECK_GL_ERROR();

  // extension for Debug in gDebugger
#ifdef GDEBUGGER
  // Get a pointer to the glStringMarkerGREMEDY function:
  if( isExtensionSupported("GL_GREMEDY_string_marker") ) 
    glStringMarkerGREMEDY = (PFNGLSTRINGMARKERGREMEDYPROC)wglGetProcAddress("glStringMarkerGREMEDY");
  else 
    glStringMarkerGREMEDY = NULL;
  // usage
  //if (glStringMarkerGREMEDY != NULL)
  //  glStringMarkerGREMEDY(0, "Start of displayCb"); 
#endif 

}

// deletes allocated buffers
void cleanup() {
  glDeleteVertexArrays(1, &Cube::vao);
  glDeleteBuffers(1, &Cube::vbo);
  glDeleteBuffers(1, &Cube::eao);
  pgr::deleteProgramAndShaders(Cube::shaderProgram);
}

void refreshCb(int) {
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);
  elapsedTime = 0.001 * (double)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

  rotAngleDeg = (float) elapsedTime * 60.0f; // rotate 60 degrees per second

  glutPostRedisplay();
}


void displayCb() {
  glm::mat4 PVMmatrix; 

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

#ifdef GDEBUGGER
  if (glStringMarkerGREMEDY != NULL)
    glStringMarkerGREMEDY(0, "Start of displayCb"); 
#endif

  glUseProgram(Cube::shaderProgram);
  glBindVertexArray( Cube::vao );

  glm::mat4 T = glm::translate(glm::mat4(1.0f), glm::vec3( 1.0f, 0.0f, 0.0f ) );
  glm::mat4 R = glm::rotate(glm::mat4(1.0f), rotAngleDeg, glm::vec3( 0.0f, 1.0f, 0.0f ) );

  // selected model transformations
  // A - complete transformation
  glm::mat4 A = cubes[selectedCubeIdx].model;
  glm::mat4 Ainv = glm::inverse(A);
          
  // B - only translation
  glm::mat4 selectedModelTranslation = glm::mat4(1.0f);
  selectedModelTranslation[3] = cubes[selectedCubeIdx].model[3];   

  // view rotation (without translation)
  glm::mat4 viewRotation = glm::inverse(view);  // INVERSE!!! - we need E, not E^(-1)
  viewRotation[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);  //without translation

  // DRAW CUBES
  for( unsigned int i = 0; i < sizeof(cubes) / sizeof(Cube); i++ ) {

    switch(selectedSpace) {
    case NO_ROTATION:
      PVMmatrix = projection * view * cubes[i].model;   // initial cube positions and rotations
      break;
    case OBJECT_SPACE:
      PVMmatrix = projection * view * cubes[i].model * R;     // each cube around its axis in object space
      break;
    case WORLD_SPACE:
      PVMmatrix = projection * view * R * cubes[i].model;     // all cubes around the world space axis (origin)
      break;
    case CAMERA_SPACE:
      PVMmatrix = projection * R * view * cubes[i].model;     // all cubes around the camera center
      break;
    case PROJECTIVE_SPACE:  
      PVMmatrix = R * projection * view * cubes[i].model;     // all cubes rotation with the frustrum
      break;
    case SELECTED_CUBE_CENTER_AND_AXIS:
      PVMmatrix = projection * view * A * R * Ainv * cubes[i].model;  // rotation in world space - around selected cube center and selected cube axis
      break;
    case SELECTED_CUBE_CENTER__CAMERA_AXIS:                    // rotation in world space - around selected cube center and camera axis
      {
      glm::mat4 B = selectedModelTranslation * viewRotation;
      glm::mat4 Binv = glm::inverse(B);
      PVMmatrix = projection * view *B * R * Binv *  cubes[i].model;  // rotation in world space - around selected cube center and camera axis
      }
      break;
    case SELECTED_CUBE_CENTER__WORLD_AXIS:                    // rotation in world space - around selected cube center and world axis
      {
      glm::mat4 B = selectedModelTranslation;
      glm::mat4 Binv = glm::inverse(B);
      PVMmatrix = projection * view *B * R * Binv *  cubes[i].model;  // rotation in world space - around selected cube center and world axis
      }
      break;
    case OBJECT_SPACE__WORLD_AXIS:                            // rotation in world space - each cube around its center and around camera axis
      {
      glm::mat4 modelTranslation = glm::mat4(1.0f);
      modelTranslation[3] = cubes[i].model[3];   

      glm::mat4 B = modelTranslation;
      glm::mat4 Binv = glm::inverse(B);
      PVMmatrix = projection * view *B * R * Binv *  cubes[i].model;  // rotation in world space - around selected cube center and camera axis
      }
      break;
    case OBJECT_SPACE__CAMERA_AXIS:                            // rotation in world space - each cube around its center and around camera axis
      {
      glm::mat4 modelTranslation = glm::mat4(1.0f);
      modelTranslation[3] = cubes[i].model[3];   

      glm::mat4 B = modelTranslation * viewRotation;
      glm::mat4 Binv = glm::inverse(B);
      PVMmatrix = projection * view *B * R * Binv *  cubes[i].model;  // rotation in world space - around selected cube center and camera axis
     
      // Simplification for view = identity
      //glm::mat4 modelTranslation = glm::mat4(1.0f);
      //modelTranslation[3] = cubes[i].model[3];
      //PVMmatrix = projection * view * modelTranslation * R;  // rotation in object space arount vertical view axis
      //break;
      }
      break;
    }

    // draw one cube
    glUniformMatrix4fv(glGetUniformLocation(Cube::shaderProgram, "PVMmatrix"), 1, GL_FALSE, glm::value_ptr(PVMmatrix));
    glDrawElements(GL_LINES, nCubeLineIdx, GL_UNSIGNED_SHORT, 0);

    char buf[255];
    snprintf(buf, 255, "After drawing of the cube [%i]", i);

#ifdef GDEBUGGER
    if (glStringMarkerGREMEDY != NULL)
      glStringMarkerGREMEDY(0, buf);
#endif 

    // draw cube axes 
    switch(selectedSpace) {
    case NO_ROTATION:
    case OBJECT_SPACE:
    case OBJECT_SPACE__CAMERA_AXIS:
    case OBJECT_SPACE__WORLD_AXIS:
      PVMmatrix = projection * view * cubes[i].model;
      glUniformMatrix4fv(glGetUniformLocation(Cube::shaderProgram, "PVMmatrix"), 1, GL_FALSE, glm::value_ptr(PVMmatrix));
      glDrawArrays(GL_LINES, 8, 6 ); 
    case SELECTED_CUBE_CENTER_AND_AXIS:
    //case SELECTED_CUBE_CENTER__CAMERA_AXIS:
    //case SELECTED_CUBE_CENTER__WORLD_AXIS:
      PVMmatrix = projection * view * cubes[selectedCubeIdx].model;
      glUniformMatrix4fv(glGetUniformLocation(Cube::shaderProgram, "PVMmatrix"), 1, GL_FALSE, glm::value_ptr(PVMmatrix));
      glDrawArrays(GL_LINES, 8, 6 ); 
    default:
      switch(selectedSpace) {
      case PROJECTIVE_SPACE:
        // draw world cube axis
        PVMmatrix = R * projection * view;
        break;
      case CAMERA_SPACE:
        PVMmatrix = projection * R * view;
        break;
      case WORLD_SPACE:
      default: 
        PVMmatrix = projection * view;
        break;
      }
      glUniformMatrix4fv(glGetUniformLocation(Cube::shaderProgram, "PVMmatrix"), 1, GL_FALSE, glm::value_ptr(PVMmatrix));
      glDrawArrays(GL_LINES, 8, 6 ); 
    }

  } // end for all cubes


  // DRAW centerpoint  
  PVMmatrix = projection * view * cubes[selectedCubeIdx].model;  // * R * T;  // circle around the selected cube center

  glUniformMatrix4fv(glGetUniformLocation(Cube::shaderProgram, "PVMmatrix"), 1, GL_FALSE, glm::value_ptr(PVMmatrix));
  glDrawArrays(GL_POINTS, 8, 1); 

  glutSwapBuffers();
}

void reshapeCb(int w, int h) {
  glViewport(0, 0, w, h);
  //projection =  glm::perspective(60.0f, float(w) / float(h) , 1.0f, 40.0f);
  projection =  glm::perspective(60.0f, float(w) / float(h) , NEAR_PLANE, FAR_PLANE);
}

void switchCube(){
  selectedCubeIdx++;
  if( selectedCubeIdx >= sizeof(cubes) / sizeof(Cube))
    selectedCubeIdx = 0;
  std::cout << "Selected cube number " << selectedCubeIdx << std::endl;
}

void reportSelectedSpace() {
  
  std::cout << "Rotation in ";

  switch(selectedSpace) {
    case NO_ROTATION:
      std::cout << "NO_ROTATION - initial cube positions and rotations" << std::endl;
      break;
    case OBJECT_SPACE:
      std::cout << "OBJECT_SPACE - each cube around its center (object space origin) and axis" << std::endl;
      break;
    case WORLD_SPACE:
      std::cout << "WORLD_SPACE - all cubes around world origin and axis" << std::endl;
      break;
    case CAMERA_SPACE:
      std::cout << "CAMERA_SPACE - all cubes around the camera" << std::endl;
      break;
    case PROJECTIVE_SPACE:
      std::cout << "PROJECTIVE_SPACE - all cubes around the frustrum 'top' " << std::endl;
      break;
    case SELECTED_CUBE_CENTER_AND_AXIS:
      std::cout << "SELECTED_CUBE_CENTER_AND_AXIS - press SPACE bar to select the cube... " << std::endl;
      break;
    case SELECTED_CUBE_CENTER__CAMERA_AXIS:
      std::cout << "SELECTED_CUBE_CENTER__CAMERA_AXIS - position = selected object center, rotation axes in camera space - press SPACE bar to select the cube... " << std::endl;
      break;
    case SELECTED_CUBE_CENTER__WORLD_AXIS:
      std::cout << "SELECTED_CUBE_CENTER__WORLD_AXIS - position = selected object center, rotation axes in world - press SPACE bar to select the cube... " << std::endl;
      break;
    case OBJECT_SPACE__WORLD_AXIS:
      std::cout<< "OBJECT_SPACE__WORLD_AXIS - each model around its center but around the world axis " << std::endl;
      break;
    case OBJECT_SPACE__CAMERA_AXIS:
      std::cout << "OBJECT_SPACE__CAMERA_AXIS - each model around its center but around the camera axis" << std::endl;
      break;
    }
}

void keyboardCb(unsigned char key, int x, int y) {
  switch (key) {
    case 27:
      glutLeaveMainLoop();
      break;
    case ' ':
      switchCube();
      break;
    case 's':
      selectedSpace = (selectedSpace + 1) % SPACE_COUNT;
      reportSelectedSpace();
      break;
    case 'S':
      selectedSpace = (selectedSpace - 1 + SPACE_COUNT) % SPACE_COUNT;
      reportSelectedSpace();
      break;
  }
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
//  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE );
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE | GLUT_DEBUG);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow(WIN_TITLE);

  glutDisplayFunc(displayCb);
  glutReshapeFunc(reshapeCb);
  glutKeyboardFunc(keyboardCb);
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);
  //glutCloseFunc(cleanup);   // for window close - use this
  glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);  // or this with cleanup after glutMainLoop()

  //if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR, pgr::DEBUG_OFF))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  init();

  std::cout << "Transformation demo" << std::endl;
  std::cout << "Use SPACE to cycle through Cubes and 'S' for coordinate space selection, Esc to quit." << std::endl;

  glutMainLoop();
  cleanup();  // needs GLUT_ACTION_ON_WINDOW_CLOSE
  return 0;
}
